pipeline {
    agent any
    tools {
        nodejs "node"
    }

    options {
        skipStagesAfterUnstable()
    }

    stages {
        stage("Add NPM Dependencies") {
            options {
                retry(3)
                timeout(activity: true, time: 60, unit: 'SECONDS')
            }
            steps {
                withNPM(npmrcConfig: env.NPM_PUBLISH_CONFIG_FILE) {
                    sh "npm install"
                }
            }
        }

        stage("Build") {
            steps {
                sh "npm run build"
            }
        }

        stage("Test") {
            options {
                lock label: "jenkins.lock.docker-swarm"
                catchError(buildResult: 'UNSTABLE', stageResult: 'FAILURE')
            }
            steps {
                sh "npm run test"
            }
        }

        stage("Publish") {
            when {
                anyOf {
                    environment name: "BRANCH_IS_PRIMARY", value: "true"
                    buildingTag()
                }
            }
            options {
                catchError(buildResult: "SUCCESS", stageResult: "ABORTED")
            }
            steps {
                withNPM(npmrcConfig: env.NPM_PUBLISH_CONFIG_FILE) {
                    sh "npm publish"
                }
            }
        }
    }

    post {
        always {
            junit checksName: 'Test Results', testResults: 'junit.xml'
        }
    }
}