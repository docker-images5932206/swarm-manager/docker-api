import Modem from "./api/Modem";
import Handle from "./utils/Handle";
import ConfigHandle, { ConfigId, ConfigSpec, Config } from "./conf/config";
import Configs, { ConfigListResult } from "./conf/configs";
import ContainerHandle, { ContainerId, ContainerSpec, Container, ContainerSummary } from "./container/container";
import Containers, { ContainerListResult } from "./container/containers";
import ImageHandle, { ImageId, Image, ImageSummary } from "./image/image";
import Images, { ImageListResult } from "./image/images";
import NetworkHandle, { NetworkId, NetworkSpec, Network } from "./network/network";
import Networks, { NetworkListResult } from "./network/networks";
import NodeHandle, { NodeId, NodeSpec, Node } from "./node/node";
import Nodes, { NodeListResult } from "./node/nodes";
import PluginHandle, { PluginId, Plugin } from "./plugin/plugin";
import Plugins, { PluginListResult } from "./plugin/plugins";
import Swarm from "./swarm/swarm";
import SecretHandle, { SecretId, SecretSpec, Secret } from "./secret/secret";
import Secrets, { SecretListResult } from "./secret/secrets";
import ServiceHandle, { ServiceId, ServiceSpec, Service, ServiceEndpointVirtualIP, ServiceConfig } from "./service/service";
import Services, { ServiceListResult } from "./service/services";
import System from "./system/system";
import TaskHandle, { TaskId, TaskSpec, Task, TaskPlacement, TaskPlacementPreference } from "./task/task";
import Tasks, { TaskListResult } from "./task/tasks";
import ClusterVolumeHandle, { ClusterVolumeId, ClusterVolumeSpec, ClusterVolume } from "./volume/clusterVolume";
import VolumeHandle, { VolumeSpec, Volume } from "./volume/volume";
import Volumes, { VolumesListResult } from "./volume/volumes";
import StackHandle, { Stack } from "./stack/stack";
import Stacks, { StackListResult } from "./stack/stacks";

export {
    Modem,
    Handle,

    System,

    ConfigListResult,
    Config,
    Configs,
    ConfigSpec,
    ConfigHandle,
    ConfigId,

    ContainerListResult,
    Container,
    ContainerSummary,
    Containers,
    ContainerSpec,
    ContainerHandle,
    ContainerId,

    ImageListResult,
    Image,
    ImageSummary,
    Images,
    ImageHandle,
    ImageId,

    NetworkListResult,
    Network,
    Networks,
    NetworkSpec,
    NetworkHandle,
    NetworkId,

    NodeListResult,
    Node,
    Nodes,
    NodeSpec,
    NodeHandle,
    NodeId,

    PluginListResult,
    Plugin,
    Plugins,
    PluginHandle,
    PluginId,

    SecretListResult,
    Secret,
    Secrets,
    SecretSpec,
    SecretHandle,
    SecretId,

    ServiceListResult,
    Service,
    Services,
    ServiceSpec,
    ServiceHandle,
    ServiceId,

    StackListResult,
    Stack,
    Stacks,
    StackHandle,

    Swarm,

    TaskListResult,
    Task,
    Tasks,
    TaskSpec,
    TaskHandle,
    TaskId,

    VolumesListResult,
    Volume,
    Volumes,
    VolumeSpec,
    VolumeHandle,
    ClusterVolume,
    ClusterVolumeSpec,
    ClusterVolumeHandle,
    ClusterVolumeId,
};

import { DockerLabels, DockerOptions, Driver, EndpointPortConfig, GenericResource, HealthConfig, Mount, Platform, IPAMConfig, DockerEvent, DockerEventActorAttributes, DockerEventScope, DockerEventType } from "./utils/types";
import { TaskContainerSecret, TaskContainerConfig } from "./task/task";
import { ClusterVolumeSecret, ClusterVolumeAccesModeScope, ClusterVolumeAccesModeSharing, ClusterVolumeAvailability } from "./volume/clusterVolume";

export {
    DockerLabels,
    DockerOptions,

    EndpointPortConfig,

    ServiceEndpointVirtualIP,
    ServiceConfig,

    TaskContainerSecret,
    TaskContainerConfig,
    TaskPlacement,
    TaskPlacementPreference,

    ClusterVolumeAccesModeScope,
    ClusterVolumeAccesModeSharing,
    ClusterVolumeAvailability,
    ClusterVolumeSecret,

    Driver,
    Mount,
    Platform,
    HealthConfig as HealthCheck,
    GenericResource,
    IPAMConfig,

    DockerEventType,
    DockerEventScope,
    DockerEventActorAttributes,
    DockerEvent
};