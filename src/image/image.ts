import Modem, { ModemDialOptions } from "../api/Modem";
import { ContainerConfig } from "../container/container";
import Handle from "../utils/Handle";
import { DockerLabels, GraphDriverData } from "../utils/types";

export type ImageId = string;

type ImageConfig = ContainerConfig;

interface ImageBase {
    Id: ImageId,
    ParentId: ImageId,
    RepoTags: Array<string>,
    RepoDigests: Array<string>,
    Created: number,
    Size: number,
    VirtualSize?: number
}

export interface ImageSummary extends ImageBase {
    SharedSize: number,
    Labels: DockerLabels,
    Containers: number
}

type ImageRootFS = {
    Type: string,
    Layers: Array<string>
};

type ImageMetadata = {
    LastTagTime: string | null
}

export interface Image extends ImageBase {
    Comment: string,
    Container: string,
    ContainerConfig: ContainerConfig,
    DockerVersion: string,
    Author: string,
    Config: ImageConfig,
    Architecture: string,
    Variant: string,
    Os: string,
    OsVersion: string,
    GraphDriver: GraphDriverData,
    RootFS: ImageRootFS,
    Metadata: ImageMetadata
}

type ImageLayer = {
    Id: string,
    Created: number,
    CreatedBy: string,
    Tags: Array<string>,
    Size: number,
    Comment: string
};

export type ImageHistoryResult = Array<ImageLayer>;

export type ImagePushQueryOptions = {
    tag?: string
};

export type ImageTagQueryOptions = {
    repo: string,
    tag: string
};

export type ImageRemoveQueryOptions = {
    force?: boolean,
    noprune?: false
};

export default class ImageHandle extends Handle {
    async inspect() {
        const call: ModemDialOptions = {
            path: "/images/" + this.id + "/json",
            method: "GET"
        };

        return await this.modem.dial(call) as Image;
    }

    async history() {
        const call: ModemDialOptions = {
            path: `/images/${this.id}/history`,
            method: "GET"
        };

        return await this.modem.dial(call) as ImageHistoryResult;
    }

    async push(queryOptions: ImagePushQueryOptions = {}, registryAuth?: string) {
        const headers = new Map();
        if (registryAuth) headers.set("X-Registry-Auth", registryAuth);

        const query = new Map();
        if (queryOptions.tag) query.set("tag", queryOptions.tag);

        const call: ModemDialOptions = {
            path: `/images/${this.id}/push`,
            method: "POST",
            headers: headers,
            query: query
        };

        return await this.modem.dial(call);
    }

    async tag(queryOptions: ImageTagQueryOptions) {
        const query = new Map();
        query.set("repo", queryOptions.repo);
        query.set("tag", queryOptions.tag);

        const call: ModemDialOptions = {
            path: `/images/${this.id}/tag`,
            method: "POST",
            query: query
        };

        return await this.modem.dial(call);
    }

    async remove(queryOptions: ImageRemoveQueryOptions = {}) {
        const query = new Map();
        if (queryOptions.force) query.set("force", queryOptions.force ? "true" : "false");
        if (queryOptions.noprune) query.set("noprune", queryOptions.noprune ? "true" : "false");

        const call: ModemDialOptions = {
            path: "/images/" + this.id,
            method: "DELETE",
            query: query
        };

        await this.modem.dial(call);
    }

    static fromImage(modem: Modem, image: Image) {
        return new ImageHandle(modem, image.Id);
    }
}