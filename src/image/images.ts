import Modem, { ModemDialOptions, ModemDialStreamResult } from "../api/Modem";
import { ContainerConfig } from "../container/container";
import HandleLogWrapper from "../utils/HandleLogWrapper";
import ImageHandle, { ImageId, ImageSummary } from "./image";

type ImageListFilterType = "before" | "dangling" | "label" | "reference" | "since" | "until";
type ImageBuildPruneFilterType = "until" | "id" | "parent" | "type" | "description" | "inuse" | "shared" | "private";
type ImageSearchFilterTypes = "is-official" | "stars";
type ImagePruneFilterType = "dangling" | "until" | "label";

export type ImageListFilters = Map<ImageListFilterType, Array<string>>;
export type ImageBuildPruneFilters = Map<ImageBuildPruneFilterType, Array<string>>;
export type ImageSearchFilters = Map<ImageSearchFilterTypes, Array<string>>;
export type ImagePruneFilters = Map<ImagePruneFilterType, Array<string>>;

export type ImageListQueryOptions = {
    all?: boolean,
    filters?: ImageListFilters,
    sharedSize?: boolean,
    digests?: boolean
};

export type ImageBuildPruneQueryOptions = {
    keepStorage: number,
    all: boolean,
    filters?: ImageBuildPruneFilters
}

export type ImagePullQueryOptions = {
    fromImage: string,
    tag?: string,
    changes?: Array<string>
    platform?: string
};

export type ImageImportQueryOptions = {
    fromSrc: string,
    repo: string,
    message?: string,
    changes?: Array<string>,
    platform?: string
};

export type ImageSearchQueryOptions = {
    term: string,
    limit: number,
    filters?: ImageSearchFilters
};

export type ImageCommitQueryOptions = {
    container?: string,
    repo: string,
    tag?: string,
    comment?: string,
    author?: string,
    pause?: boolean,
    changes?: string
};

export type ImageListResult = Array<ImageSummary>;

export type ImageBuildPruneResult = {
    CachesDeleted: Array<string>,
    SpaceReclaimed: number
};

export type ImagePruneResultImageDeleted = {
    Untagged: string,
    Deleted: string
};

export type ImageSearchResult = Array<{
    description: string,
    is_official: boolean,
    name: string,
    star_count: number
}>;

export type ImagePruneResult = {
    ImagesDeleted: Array<ImagePruneResultImageDeleted>,
    SpaceReclaimed: number
};

export type ImageCommitResult = {
    Id: string
};

export default class Images {
    static async list(modem: Modem, queryOptions: ImageListQueryOptions = {}) {
        const query = new Map();
        if (queryOptions.all) query.set("all", queryOptions.all ? "true" : "false");
        if (queryOptions.filters) query.set("filters", JSON.stringify(Object.fromEntries(queryOptions.filters)));
        if (queryOptions.sharedSize) query.set("shared-size", queryOptions.sharedSize ? "true" : "false");
        if (queryOptions.digests) query.set("digests", queryOptions.digests ? "true" : "false");

        const call: ModemDialOptions = {
            path: "/images/json",
            method: "GET",
            query: query
        };

        return await modem.dial(call) as ImageListResult;
    }

    // TODO build image

    static async pruneBuilderCache(modem: Modem, queryOptions: ImageBuildPruneQueryOptions) {
        const query = new Map();
        query.set("keep-storage", queryOptions.keepStorage);
        query.set("all", queryOptions.all ? "true" : "false");
        if (queryOptions.filters) query.set("filters", JSON.stringify(Object.fromEntries(queryOptions.filters)));

        const call: ModemDialOptions = {
            path: "/build/prune",
            method: "POST",
            query: query
        };

        return await modem.dial(call) as ImageBuildPruneResult;
    }

    /**
     * @see https://docs.docker.com/reference/api/engine/version/v1.44/#tag/Image/operation/ImageCreate
     * 
     * @param modem 
     * @param queryOptions 
     * @param registryAuth 
     * @returns 
     */
    static async pull(modem: Modem, queryOptions: ImagePullQueryOptions, registryAuth?: string) {
        const headers = new Map();
        if (registryAuth) headers.set("X-Registry-Auth", registryAuth);

        const query = new Map();
        query.set("fromImage", queryOptions.fromImage);

        if (queryOptions.tag) query.set("tag", queryOptions.tag);
        if (queryOptions.changes) query.set("changes", queryOptions.changes);
        if (queryOptions.platform) query.set("platform", queryOptions.platform);

        const call: ModemDialOptions = {
            path: "/images/create",
            method: "POST",
            headers: headers,
            query: query,
            isStream: true
        };

        const stream = await modem.dial(call) as ModemDialStreamResult;
        const handle = new ImageHandle(modem, queryOptions.fromImage);
        const logWrapper = new HandleLogWrapper(handle, stream);

        await logWrapper.drainLogStream();
        return logWrapper;
    }

    /**
     * @see https://docs.docker.com/reference/api/engine/version/v1.44/#tag/Image/operation/ImageCreate
     * 
     * @param modem 
     * @param queryOptions 
     * @param image 
     * @returns 
     */
    static async import(modem: Modem, queryOptions: ImageImportQueryOptions, image?: string) {
        const query = new Map();
        query.set("fromSrc", queryOptions.fromSrc);
        query.set("repo", queryOptions.repo);

        if (queryOptions.message) query.set("message", queryOptions.message);
        if (queryOptions.changes) query.set("changes", queryOptions.changes);
        if (queryOptions.platform) query.set("platform", queryOptions.platform);

        const call: ModemDialOptions = {
            path: "/images/create",
            method: "POST",
            query: query,
            body: image,
            isStream: true
        };

        const stream = await modem.dial(call) as ModemDialStreamResult;
        const handle = new ImageHandle(modem, queryOptions.repo);
        const logWrapper = new HandleLogWrapper(handle, stream);

        await logWrapper.drainLogStream();
        return logWrapper;
    }

    static async search(modem: Modem, queryOptions: ImageSearchQueryOptions) {
        const query = new Map();
        query.set("term", queryOptions.term);
        query.set("limit", queryOptions.limit);
        if (queryOptions.filters) query.set("filters", JSON.stringify(Object.fromEntries(queryOptions.filters)));

        const call: ModemDialOptions = {
            path: "/images/search",
            method: "GET",
            query: query
        };

        return await modem.dial(call) as ImageSearchResult;
    }

    static async prune(modem: Modem, filters?: ImagePruneFilters) {
        const query = new Map();
        if (filters) query.set("filters", JSON.stringify(Object.fromEntries(filters)));

        const call: ModemDialOptions = {
            path: "/images/prune",
            method: "POST",
            query: query
        };

        return await modem.dial(call) as ImagePruneResult;
    }

    /**
     * Creates a new image based on a container configuration
     * @param modem the modem to communicate with docker
     * @param queryOptions query specific options
     * @param containerConfig configuration of the container for the image
     */
    static async commit(modem: Modem, queryOptions: ImageCommitQueryOptions, containerConfig?: ContainerConfig) {
        if (!containerConfig && !queryOptions.container)
            throw new Error("Either containerConfig or queryOption 'container' must be specified");

        const query = new Map();
        query.set("repo", queryOptions.repo);
        query.set("tag", queryOptions.tag ?? "");
        query.set("comment", queryOptions.comment ?? "");
        query.set("author", queryOptions.author ?? "");
        query.set("pause", queryOptions.pause ? "true" : "false");

        if (queryOptions.container) query.set("container", queryOptions.container);
        if (queryOptions.changes) query.set("changes", queryOptions.changes);

        const call: ModemDialOptions = {
            path: "/commit",
            method: "POST",
            query: query,
            body: containerConfig
        };

        return await modem.dial(call) as ImageCommitResult;
    }

    // TODO export image
    // TODO export multiple images
    // TODO import multiple images
    
    static getHandle(modem: Modem, id: ImageId) {
        return new ImageHandle(modem, id);
    }
}