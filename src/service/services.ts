import Modem, { ModemDialOptions } from "../api/Modem";
import ServiceHandle, { Service, ServiceId, ServiceSpec } from "./service";

type ServiceListFilterType = "id" | "label" | "mode" | "name";

export type ServiceListFilters = Map<ServiceListFilterType, Array<string>>;
export type ServiceListResult = Array<Service>;

export type ServiceListQueryOptions = {
    filters?: ServiceListFilters
    status?: boolean
};

type ServiceCreateResult = {
    ID: ServiceId,
    Warnings: Array<string>
};

export default class Services {
    /**
     * @see https://docs.docker.com/engine/api/v1.44/#tag/Service/operation/ServiceList
     * 
     * @param modem the modem to use 
     * @param filters the filters to use 
     * @param status include service status 
     */
    static async list(modem: Modem, queryOptions: ServiceListQueryOptions = {}) {
        const status = typeof queryOptions.status === "undefined" ? true : queryOptions.status;
        
        const query = new Map();
        query.set("status", status ? "true" : "false");
        if (queryOptions.filters) query.set("filters", JSON.stringify(Object.fromEntries(queryOptions.filters)));

        const call: ModemDialOptions = {
            path: "/services",
            method: "GET",
            query: query
        };

        return await modem.dial(call) as ServiceListResult;
    }

    static async create(modem: Modem, spec: ServiceSpec, registryAuth?: string) {
        const headers = new Map();
        if (registryAuth) headers.set("X-Registry-Auth", registryAuth);

        const call: ModemDialOptions = {
            path: "/services/create",
            method: "POST",
            headers: headers,
            body: spec
        };

        const result = await modem.dial(call) as ServiceCreateResult;
        return new ServiceHandle(modem, result.ID);
    }

    static getHandle(modem: Modem, id: ServiceId) {
        return new ServiceHandle(modem, id);
    }
}