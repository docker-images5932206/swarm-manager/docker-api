import Modem, { ModemDialOptions, ModemDialStreamResult } from "../api/Modem";
import { NetworkId } from "../network/network";
import { DOCKER_STACK_NAMESPACE_LABEL } from "../stack/stackDeps";
import { TaskSpec } from "../task/task";
import Handle from "../utils/Handle";
import Stackable from "../utils/Stackable";
import { ALLXOR, DockerLabels, EndpointPortConfig, EndpointSpec, NetworkAttachementConfig, ObjectVersion, SwarmBase } from "../utils/types";
import Services from "./services";

export type ServiceId = string;

type ServiceModeReplicated = {
    Replicas: number
};

type ServiceModeReplicatedJob = {
    MaxConcurrent: number, // default 1
    TotalCompletions: number
};

type ServiceModeReplicatedWrapper = {
    Replicated: ServiceModeReplicated
};
type ServiceModeReplicatedJobWrapper = {
    ReplicatedJob: ServiceModeReplicatedJob
}
type ServiceModeGlobalWrapper = {
    Global: object;
};
type ServiceModeGlobalJobWrapper = {
    GlobalJob: object;
};

type ServiceMode = ALLXOR<[ServiceModeReplicatedWrapper, ServiceModeReplicatedJobWrapper, ServiceModeGlobalWrapper, ServiceModeGlobalJobWrapper]>;

type ServiceConfigOrder = "stop-first" | "start-first";
export interface ServiceConfig<FailureActionType extends string> {
    Parallelism: number, // 0 = unlimited
    Delay: number,
    FailureAction: FailureActionType,
    Monitor: number,
    MaxFailureRatio: number,
    Order: ServiceConfigOrder
}

type ServiceUpdateConfigFailureAction = "continue" | "pause" | "rollback";
type ServiceRollbackConfigFailureAction = "continue" | "pause";

export type ServiceSpec = {
    Name?: string,
    Labels?: DockerLabels,
    TaskTemplate: TaskSpec,
    Mode?: ServiceMode,
    UpdateConfig?: ServiceConfig<ServiceUpdateConfigFailureAction>,
    RollbackConfig?: ServiceConfig<ServiceRollbackConfigFailureAction>,
    Networks?: Array<NetworkAttachementConfig>,
    EndpointSpec?: EndpointSpec,
};

export type ServiceEndpointVirtualIP = {
    NetworkID: NetworkId,
    Addr: string
};

type ServiceEndpoint = {
    Spec: EndpointSpec,
    Ports?: Array<EndpointPortConfig>,
    VirtualIPs?: Array<ServiceEndpointVirtualIP>
};

type ServiceUpdateStatusState = "updating" | "paused" | "completed";
type ServiceUpdateStatus = {
    State: ServiceUpdateStatusState,
    StartedAt: string,
    CompletedAt: string,
    Message: string
};

type ServiceServiceStatus = {
    RunningTasks: number,
    DesiredTasks: number,
    CompletedTasks: number
};

type ServiceJobStatus = {
    JobIteration: ObjectVersion,
    LastExecution: string
};

export interface Service extends SwarmBase<ServiceId> {
    Spec: ServiceSpec,
    PreviousSpec?: ServiceSpec,
    Endpoint: ServiceEndpoint,
    UpdateStatus: ServiceUpdateStatus,
    ServiceStatus: ServiceServiceStatus,
    JobStatus: ServiceJobStatus
};

export type ServiceInspectQueryOptions = {
    insertDefaults?: boolean
};

export type ServiceLogsQueryOptions = {
    details?: boolean,
    follow?: boolean,
    stdout?: boolean,
    stderr?: boolean,
    since?: number,
    timestamps?: boolean,
    tail?: number | "all"
};

type ServiceUpdateQueryOptionsRegistryAuthFrom = "spec" | "previous-spec";
export type ServiceRollbackQueryOptions = {
    registryAuthFrom?: ServiceUpdateQueryOptionsRegistryAuthFrom
};

export type ServiceUpdateQueryOptions = ServiceRollbackQueryOptions & {
    rollback?: string
};

type ServiceUpdateResult = {
    Warnings: Array<string>
};

export default class ServiceHandle extends Handle implements Stackable {
    async inspect(queryOptions: ServiceInspectQueryOptions = {}) {
        const query = new Map();
        query.set("insertDefaults", queryOptions.insertDefaults ? "true" : "false");

        const call: ModemDialOptions = {
            path: "/services/" + this.id,
            method: "GET",
            query: query
        };

        return await this.modem.dial(call) as Service;
    }

    /**
     * @see https://docs.docker.com/engine/api/v1.44/#tag/Service/operation/ServiceLogs
     * 
     * @param queryOptions the query options to use
     */
    async getLogs(queryOptions: ServiceLogsQueryOptions = {}) {
        const query = new Map();
        query.set("details", queryOptions.details ? "true" : "false");
        query.set("follow", queryOptions.follow ? "true" : "false");
        query.set("stdout", queryOptions.stdout ? "true" : "false");
        query.set("stderr", queryOptions.stdout ? "true" : "false");
        query.set("since", queryOptions.since || 0);
        query.set("timestamps", queryOptions.timestamps ? "true" : "false");
        query.set("tail", queryOptions.tail || "all");

        const call: ModemDialOptions = {
            path: "/services/" + this.id + "/logs",
            method: "GET",
            query: query
        };

        return await this.modem.dial(call) as ModemDialStreamResult;
    }

    /**
     * @see https://docs.docker.com/engine/api/v1.44/#tag/Service/operation/ServiceUpdate
     * 
     * @param newSpec the new spec for the service
     * @param queryOptions the query options to use
     * @param registryAuth the authentication to connect to a private registry if needed
     */
    async update(newSpec: ServiceSpec, queryOptions: ServiceUpdateQueryOptions = {}, registryAuth?: string) {
        const current = await this.inspect();
        const version = current.Version.Index;

        const headers = new Map();
        if (registryAuth) headers.set("X-Registry-Auth", registryAuth);

        const query = new Map();
        query.set("version", version);
        query.set("registryAuthFrom", queryOptions.registryAuthFrom || "spec");

        if (queryOptions.rollback) query.set("rollback", queryOptions.rollback);

        const body = newSpec;
        const call: ModemDialOptions = {
            path: "/services/" + this.id + "/update",
            method: "POST",
            headers: headers,
            query: query,
            body: body
        };

        return await this.modem.dial(call) as ServiceUpdateResult;
    }

    /**
     * Removes this service and creates a new one with the same spec
     * @param registryAuth 
     */
    async redeploy(registryAuth?: string) {
        const current = await this.inspect({ insertDefaults: true });

        if (this.shouldThrowStackError() && ServiceHandle.isPartOfStack(current))
            throw new Error("The current service was created by a stack, consider redeploying the stack");

        const spec = current.Spec;

        await this.remove();
        return await Services.create(this.modem, spec, registryAuth);
    }

    async rollback(queryOptions: ServiceRollbackQueryOptions = { registryAuthFrom: "spec" }, registryAuth?: string) {
        const current = await this.inspect();

        const rollbackQueryOptions: ServiceUpdateQueryOptions = Object.assign(queryOptions, {
            rollback: "previous"
        });

        return await this.update(current.Spec, rollbackQueryOptions, registryAuth);
    }

    async stop() {
        const current = await this.inspect();
        const spec = current.Spec;

        if (spec.Mode?.Replicated && spec.Mode.Replicated.Replicas > 0) {
            spec.Mode.Replicated.Replicas = 0;

            return await this.update(spec);
        } else if (spec.Mode?.Replicated && spec.Mode.Replicated.Replicas <= 0)
            throw new Error("This service is already stoppped");

        throw new Error("The service is not a replicated service and can not be stopped");
    }

    /**
     * This will start a stopped service.  
     * Be careful tho, because it does a server side redeploy to the last running spec.  
     * And it only does that if the service is not running
     */
    async start() {
        const current = await this.inspect();
        const spec = current.Spec;
        const prev = current.PreviousSpec;

        if (prev && prev.Mode?.Replicated && prev.Mode.Replicated.Replicas > 0 && spec.Mode?.Replicated && spec.Mode.Replicated.Replicas <= 0)
            return await this.rollback();
        else if (prev && prev.Mode?.Replicated && prev.Mode.Replicated.Replicas > 0 &&  spec.Mode?.Replicated && spec.Mode.Replicated.Replicas > 0)
            throw new Error("This service is not stoppped");
        else if (prev && prev.Mode?.Replicated && prev.Mode.Replicated.Replicas <= 0)
            throw new Error("This service can not be started, scale it to any number");
        else if (!prev)
            throw new Error("This service never started, scale it to any number first");
        else
            throw new Error("Unexpected error");
    }

    async remove() {
        const current = await this.inspect({ insertDefaults: true });

        if (this.shouldThrowStackError() && ServiceHandle.isPartOfStack(current))
            throw new Error("The current service was created by a stack, it can not be removed individually");

        const call: ModemDialOptions = {
            path: "/services/" + this.id,
            method: "DELETE"
        };

        return await this.modem.dial(call);
    }

    public shouldThrowStackError() {
        return true;
    }

    static isPartOfStack(service: Service) {
        return service.Spec.Labels && DOCKER_STACK_NAMESPACE_LABEL in service.Spec.Labels;
    }

    static fromService(modem: Modem, service: Service) {
        const id = service.ID;
        return new ServiceHandle(modem, id);
    }
}