import VolumeHandle, { Volume, VolumeRemoveQueryOptions, VolumeSpec } from "./volume";
import Modem, { ModemDialOptions } from "../api/Modem";
import { SwarmBase, Topology, XOR } from "../utils/types";
import { DOCKER_STACK_NAMESPACE_LABEL } from "../stack/stackDeps";

export type ClusterVolumeId = string;

export type ClusterVolumeAccesModeScope = "single" | "multi";
export type ClusterVolumeAccesModeSharing = "none" | "readonly" | "onewriter" | "all";
export type ClusterVolumeAvailability = "active" | "pause" | "drain";
type ClusterVolumeDataPublishStatusState = "pending-publish" | "published" | "pending-node-unpublish" | "pending-controller-unpublish";

type ClusterVolumeAccessModeBase = {
    Scope?: ClusterVolumeAccesModeScope,
    Sharing?: ClusterVolumeAccesModeSharing
};

type ClusterVolumeAccessModeMountVolume = { MountVolume: object };
type ClusterVolumeAccessModeBlockVolume = { BlockVolume: object };

type ClusterVolumeAccessMode = ClusterVolumeAccessModeBase & XOR<ClusterVolumeAccessModeMountVolume, ClusterVolumeAccessModeBlockVolume>;
export type ClusterVolumeSecret = {
    Key: string,
    Secret: string
};

type ClusterVolumeAccessibilityRequirements = {
    Requisite: Array<object>,
    Preferred: Array<object>
};

type ClusterVolumeCapacityRange = {
    RequiredBytes: number,
    LimitBytes: number
};

type ClusterVolumeSpecBase = {
    Group?: string,
    AccessMode: ClusterVolumeAccessMode,
    Secrets?: Array<ClusterVolumeSecret>,
    AccessibilityRequirements?: ClusterVolumeAccessibilityRequirements,
    CapacityRange?: ClusterVolumeCapacityRange,
    Availability?: ClusterVolumeAvailability
};

type ClusterVolumeDataInfo = {
    CapacityBytes: number,
    VolumeContext: object,
    VolumeID: string,
    AccessibleTopology: Array<Topology>
};
type ClusterVolumeDataPublishStatusContext = {
    [name: string]: string
};
type ClusterVolumeDataPublishStatus = {
    NodeID: string,
    State: ClusterVolumeDataPublishStatusState,
    PublishContext: ClusterVolumeDataPublishStatusContext
};

interface ClusterVolumeData extends SwarmBase<ClusterVolumeId> {
    Spec: ClusterVolumeSpecBase,
    Info: ClusterVolumeDataInfo,
    PublishStatus: Array<ClusterVolumeDataPublishStatus>
}

export interface ClusterVolumeSpec extends VolumeSpec {
    ClusterVolumeSpec: ClusterVolumeSpecBase
}

export interface ClusterVolume extends Volume {
    ClusterVolume: ClusterVolumeData
}

export default class ClusterVolumeHandle extends VolumeHandle {
    async inspect() {
        return await super.inspectResult() as ClusterVolume;
    }

    /**
     * CHECK when docker implements options to change more settings maybe update this to accept a ClusterVolumeSpec object
     */
    async update(spec: ClusterVolumeSpec) {
        const current = await this.inspect();
        const version = current.ClusterVolume.Version.Index;
        const body = {
            Spec: current.ClusterVolume.Spec
        };

        const query = new Map();
        query.set("version", version.toString());

        body.Spec.Availability = spec.ClusterVolumeSpec.Availability;

        const call: ModemDialOptions = {
            path: "/volumes/" + this.id,
            method: "PUT",
            query: query,
            body: body
        };

        await this.modem.dial(call);
    }

    async remove() {
        const current = await this.inspect();

        if (this.shouldThrowStackError() && ClusterVolumeHandle.isPartOfStack(current))
            throw new Error("The current cluster-volume was created by a stack, it can not be removed individually");

        const options: VolumeRemoveQueryOptions = {
            force: true
        };

        await super.remove(options);
    }

    static isPartOfStack(volume: ClusterVolume) {
        return volume.Labels && DOCKER_STACK_NAMESPACE_LABEL in volume.Labels;
    }

    static fromClusterVolume(modem: Modem, clusterVolume: ClusterVolume) {
        const id = clusterVolume.ClusterVolume.ID;
        return new ClusterVolumeHandle(modem, id);
    }
}