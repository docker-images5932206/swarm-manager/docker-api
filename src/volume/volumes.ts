import Modem, { ModemDialOptions } from "../api/Modem";
import ClusterVolumeHandle, { ClusterVolume, ClusterVolumeSpec } from "./clusterVolume";
import VolumeHandle, { Volume, VolumeSpec } from "./volume";

type VolumeListFilterType = "dangling" | "driver" | "label" | "name";
type VolumePruneFilterType = "label" | "all";

export type VolumeListFilters = Map<VolumeListFilterType, Array<string>>;
export type VolumePruneFilters = Map<VolumePruneFilterType, Array<string>>;

export type VolumeListQueryOptions = {
    filters?: VolumeListFilters
};

export type VolumePruneQueryOptions = {
    filters?: VolumePruneFilters
};

export type VolumesListResult<ListType extends Volume | ClusterVolume = Volume | ClusterVolume> = {
    Volumes: Array<ListType>,
    Warnings: Array<string>
}

type VolumePruneResult = {
    VolumesDeleted: Array<string>,
    SpaceReclaimed: number
}

export default class Volumes {
    private static async _list<ListType extends Volume | ClusterVolume>(modem: Modem, typeFilter: (volume: Volume | ClusterVolume) => boolean, queryOptions: VolumeListQueryOptions = {}) {
        const query = new Map();
        if (queryOptions.filters) query.set("filters", JSON.stringify(Object.fromEntries(queryOptions.filters)));

        const call: ModemDialOptions = {
            path: "/volumes",
            method: "GET",
            query: query
        };

        const result = await modem.dial(call) as VolumesListResult<ListType>;
        result.Volumes = result.Volumes.filter(typeFilter);
        return result;
    }

    private static async _create<T extends VolumeSpec>(modem: Modem, spec: T) {
        const call: ModemDialOptions = {
            path: "/volumes/create",
            method: "POST",
            body: spec
        };

        return await modem.dial(call);
    }
    
    /**
     * @see https://docs.docker.com/engine/api/v1.44/#tag/Volume/operation/VolumeList
     * 
     * @param modem the modem to use
     * @param filters a map of filters to use
     * @returns 
     */
    static async list(modem: Modem, queryOptions: VolumeListQueryOptions = {}) {
        const typeFilter = (volume: Volume | ClusterVolume) => !("ClusterVolume" in volume);
        return Volumes._list<Volume>(modem, typeFilter, queryOptions);
    }

    static async listCluster(modem: Modem, queryOptions: VolumeListQueryOptions = {}) {
        const typeFilter = (volume: Volume | ClusterVolume) => "ClusterVolume" in volume;
        return Volumes._list<ClusterVolume>(modem, typeFilter, queryOptions);
    }

    static async create(modem: Modem, spec: VolumeSpec) {
        const result = await Volumes._create(modem, spec) as Volume;
        return new VolumeHandle(modem, result.Name);
    }

    static async createCluster(modem: Modem, spec: ClusterVolumeSpec) {
        const result = await Volumes._create(modem, spec) as ClusterVolume;
        return new ClusterVolumeHandle(modem, result.ClusterVolume.ID);
    }

    static async prune(modem: Modem, queryOptions: VolumePruneQueryOptions = {}) {
        const query = new Map();
        if (queryOptions.filters) query.set("filters", JSON.stringify(Object.fromEntries(queryOptions.filters)));

        const call: ModemDialOptions = {
            path: "/volumes/prune",
            method: "POST",
            query: query
        };

        return await modem.dial(call) as VolumePruneResult;
    }

    static getHandle(modem: Modem, name: string) {
        return new VolumeHandle(modem, name);
    }

    static getClusterHandle(modem: Modem, name: string) {
        return new ClusterVolumeHandle(modem, name);
    }
}