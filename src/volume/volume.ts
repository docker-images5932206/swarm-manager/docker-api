import Modem, { ModemDialOptions } from "../api/Modem";
import { DOCKER_STACK_NAMESPACE_LABEL } from "../stack/stackDeps";
import Handle from "../utils/Handle";
import { DockerOptions, DockerLabels } from "../utils/types";

type DockerVolumeStatus = {
    [key: string]: string
};

type VolumeScope = "local" | "global";
type VolumeUsageData = {
    Size: number,
    RefCount: number
};

export interface VolumeSpec {
    Name?: string,
    Driver?: string,
    DriverOpts?: DockerOptions,
    Labels?: DockerLabels
}

export interface Volume {
    Name: string,
    Driver: string,
    Mountpoint: string,
    CreatedAt: string,
    Status: DockerVolumeStatus,
    Labels: DockerLabels,
    Scope: VolumeScope,
    Options: DockerOptions,
    UsageData: VolumeUsageData | null
}

export type VolumeRemoveQueryOptions = {
    force?: boolean
};

export default class VolumeHandle extends Handle {
    constructor(modem: Modem, name: string) {
        super(modem, name);
    }

    protected async inspectResult() {
        const query = new Map();
        query.set("name", this.id);

        const call: ModemDialOptions = {
            path: "/volumes/" + this.id,
            method: "GET",
            query: query
        };

        return await this.modem.dial(call);
    }

    async inspect() {
        return await this.inspectResult() as Volume;
    }

    async remove(queryOptions: VolumeRemoveQueryOptions = {}) {
        const current = await this.inspect();
        
        if (this.shouldThrowStackError() && VolumeHandle.isPartOfStack(current))
            throw new Error("The current volume was created by a stack, it can not be removed individually");

        const query = new Map();
        query.set("force", queryOptions.force ? "true" : "false");

        const call: ModemDialOptions = {
            path: "/volumes/" + this.id,
            method: "DELETE",
            query: query
        };

        await this.modem.dial(call);
    }

    protected shouldThrowStackError() {
        return true;
    }

    static isPartOfStack(volume: Volume) {
        return volume.Labels && DOCKER_STACK_NAMESPACE_LABEL in volume.Labels;
    }

    static fromVolume(modem: Modem, volume: Volume) {
        const id = volume.Name;
        return new VolumeHandle(modem, id);
    }
}