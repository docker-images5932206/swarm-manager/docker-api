import Modem, { ModemDialOptions } from "../api/Modem";
import TaskHandle, { Task, TaskId } from "./task";

type TaskListFilterType = "desired-state" | "id" | "label" | "name" | "node" | "service";
export type TaskListFilters = Map<TaskListFilterType, Array<string>>;

export type TaskListQueryOptions = {
    filters?: TaskListFilters
};

export type TaskListResult = Array<Task>;

export default class Tasks {
    static async list(modem: Modem, queryOptions: TaskListQueryOptions = {}) {
        const query = new Map();
        if (queryOptions.filters) query.set("filters", JSON.stringify(Object.fromEntries(queryOptions.filters)));

        const call: ModemDialOptions = {
            path: "/tasks",
            method: "GET",
            query: query
        };

        return await modem.dial(call) as TaskListResult;
    }

    static getHandle(modem: Modem, id: TaskId) {
        return new TaskHandle(modem, id);
    }
}