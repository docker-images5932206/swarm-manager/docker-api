import Modem, { ModemDialOptions, ModemDialStreamResult } from "../api/Modem";
import { ContainerId, ContainerStatus } from "../container/container";
import { NodeId } from "../node/node";
import { ServiceId } from "../service/service";
import Handle from "../utils/Handle";
import { ALLXOR, DockerLabels, DockerResources, HealthConfig, Driver, Mount, NetworkAttachementConfig, Platform, PluginPrivilege, Isolation, ObjectVersion, GenericResource, PortStatus, XOR, SwarmBase } from "../utils/types";

export type TaskId = string;

type TaskPluginSpec = {
    Name: string,
    Remote: string,
    Disabled: boolean,
    PluginPrivileges: Array<PluginPrivilege>
};

type TaskContainerPrivilegesCredentialSpec = {
    Config: string,
    File: string,
    Registry: string
};

type TaskContainerPrivilegesSELinuxContext = {
    Disable: boolean,
    User: string,
    Role: string,
    Type: string,
    Level: string
};

type TaskContainerPrivilegesSeccompMode = "default" | "unconfined" | "custom";
type TaskContainerPrivilegesSeccomp = {
    Mode: TaskContainerPrivilegesSeccompMode,
    Profile: string
};

type TaskContainerPrivilegesAppArmorMode = "default" | "disabled";
type TaskContainerPrivilegesAppArmor = {
    Mode: TaskContainerPrivilegesAppArmorMode
};

type TaskContainerPrivileges = {
    CredentialSpec: TaskContainerPrivilegesCredentialSpec,
    SELinuxContext: TaskContainerPrivilegesSELinuxContext,
    Seccomp: TaskContainerPrivilegesSeccomp,
    AppArmor: TaskContainerPrivilegesAppArmor,
    NoNewPrivileges: boolean
};

type TaskContainerDNSConfig = {
    NameServers: Array<string>,
    Search: Array<string>,
    Options: Array<string>
};

type TaskContainerFile = {
    Name: string,
    UID: string,
    GID: string,
    Mode: number
};

export type TaskContainerSecret = {
    File: TaskContainerFile,
    SecretID: string,
    SecretName: string
};

type TaskContainerConfigFile = { File: TaskContainerFile };
type TaskContainerConfigRuntime = { Runtime: object };

type TaskContainerConfigBase = {
    ConfigID: string,
    ConfigName: string
}

export type TaskContainerConfig = XOR<TaskContainerConfigFile, TaskContainerConfigRuntime> & TaskContainerConfigBase;

type TaskContainerSysctls = {
    [name: string]: string
};

type TaskContainerUlimit = {
    Name: string,
    Soft: number,
    Hard: number
};

type TaskContainerSpec = {
    Image: string,
    Labels?: DockerLabels,
    Command?: Array<string>,
    Args?: Array<string>,
    Hostname?: string,
    Env?: Array<string>,
    Dir?: string,
    User?: string,
    Groups?: Array<string>,
    Privileges?: TaskContainerPrivileges,
    TTY?: boolean,
    OpenStdin?: boolean,
    ReadOnly?: boolean,
    Mounts?: Array<Mount>,
    StopSignal?: string,
    StopGracePeriod?: number,
    HealthCheck?: HealthConfig,
    Hosts?: Array<string>,
    DNSConfig?: TaskContainerDNSConfig,
    Secrets?: Array<TaskContainerSecret>,
    Configs?: Array<TaskContainerConfig>,
    Isolation?: Isolation,
    Init?: boolean | null,
    Sysctls?: TaskContainerSysctls,
    CapabilityAdd?: Array<string>,
    CapabilityDrop?: Array<string>,
    Ulimits?: Array<TaskContainerUlimit>
};

type TaskNetworkAttachmentSpec = {
    ContainerID: ContainerId
};

type TaskRestartPolicyCondition = "none" | "on-failure" | "any";
type TaskRestartPolicy = {
    Condition: TaskRestartPolicyCondition,
    Delay: number,
    MaxAttempts: number, // 0 = ignored
    Window: number // 0 = unbound
};

type TaskPlacementPreferenceSpread = {
    SpreadDescriptor: string
};

export type TaskPlacementPreference = {
    Spread: TaskPlacementPreferenceSpread
};

/**
 * @see https://docs.docker.com/engine/api/v1.44/#tag/Service/operation/ServiceList
 */
export type TaskPlacement = {
    Constraints: Array<string>,
    Preferences: Array<TaskPlacementPreference>,
    MaxReplicas: number, // 0 = unlimited
    Platforms: Array<Platform>
};

type TaskSpecBase = {
    Resources?: DockerResources,
    RestartPolicy?: TaskRestartPolicy,
    Placement?: TaskPlacement,
    ForceUpdate?: number,
    Runtime?: string,
    Networks?: Array<NetworkAttachementConfig>,
    LogDriver?: Driver
};

type PluginTaskSpec = { PluginSpec: TaskPluginSpec };
type ContainerTaskSpec = { ContainerSpec: TaskContainerSpec };
type NetworkAttachmentTaskSpec = { NetworkAttachmentSpec: TaskNetworkAttachmentSpec };

export type TaskSpec = ALLXOR<[PluginTaskSpec, ContainerTaskSpec, NetworkAttachmentTaskSpec]> & TaskSpecBase;

type TaskState = "new" | "allocated" | "pending" | "assigned" | "accepted" | "preparing" | "ready" | "starting" | "running" | "complete" | "shutdown" | "failed" | "rejected" | "remove" | "orphaned";
type TaskStatus = {
    Timestamp: string,
    State: TaskState,
    Message: string,
    Err: string,
    ContainerStatus?: ContainerStatus,
    PortStatus: PortStatus,
};

export interface Task extends SwarmBase<TaskId> {
    Name: string,
    Labels: DockerLabels,
    Spec: TaskSpec,
    ServiceID: ServiceId,
    Slot: number,
    NodeID: NodeId,
    AssignedGenericResources: Array<GenericResource>,
    Status: TaskStatus,
    Desired: TaskState,
    JobIteration: ObjectVersion
}

export type TaskLogsQueryOptions = {
    details?: boolean,
    follow?: boolean,
    stdout?: boolean,
    stderr?: boolean,
    since?: number,
    timestamps?: boolean,
    tail?: string | "all"
};

export default class TaskHandle extends Handle {
    async inspect() {
        const call: ModemDialOptions = {
            path: "/tasks/" + this.id,
            method: "GET"
        };

        return await this.modem.dial(call) as Task;
    }

    async getLogs(queryOptions: TaskLogsQueryOptions = {}) {
        const query = new Map();
        query.set("details", queryOptions.details ? "true" : "false");
        query.set("follow", queryOptions.follow ? "true" : "false");
        query.set("stdout", queryOptions.stdout ? "true" : "false");
        query.set("stderr", queryOptions.stderr ? "true" : "false");
        query.set("since", queryOptions.since ? queryOptions.since : 0);
        query.set("timestamps", queryOptions.timestamps ? "true" : "false");
        query.set("tail", queryOptions.tail);

        const call: ModemDialOptions = {
            path: "/tasks/" + this.id + "/logs",
            method: "GET",
            query: query
        };

        return await this.modem.dial(call) as ModemDialStreamResult;
    }

    static fromTask(modem: Modem, task: Task) {
        const id = task.ID;
        return new TaskHandle(modem, id);
    }
}