# Tasks

INFO: it can take a few seconds to delete tasks when a service is removed so any error that might come up in this context might be solved by waiting a small amount of time.