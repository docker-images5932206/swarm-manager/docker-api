import Modem from "../api/Modem";

export default abstract class Handle {
    protected readonly modem: Modem;
    public readonly id: string;

    constructor(modem: Modem, id: string) {
        this.modem = modem;
        this.id = id;
    }
}