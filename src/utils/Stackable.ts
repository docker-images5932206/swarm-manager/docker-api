export default interface Stackable {
    shouldThrowStackError(): boolean;
}