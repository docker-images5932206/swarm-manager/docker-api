import Modem, { ModemDialStreamResult } from "../api/Modem";
import Handle from "./Handle";

export default class HandleLogWrapper<HandleType extends Handle> {
    private readonly handle: HandleType;
    private readonly logStream?: ModemDialStreamResult;
    private readonly logChunks: Array<string>;

    public constructor(handle: HandleType, logStream?: ModemDialStreamResult) {
        this.handle = handle;
        this.logStream = logStream;
        this.logChunks = new Array();
    }

    public async drainLogStream() {
        if (!this.logStream)
            return;
        
        const handle = this;
        const stream = this.logStream;

        await new Promise(function(resolve) {
            stream.on("data", function(data) { handle.logChunks.push(data.toString()); });
            stream.on("end", resolve);
        });
    }

    public get Handle() { return this.handle; }
    public get LogChunks() { return this.logChunks; }
}