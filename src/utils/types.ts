export type WITHOUT<T extends any> = { [P in keyof T]?: undefined };
export type XOR<T extends any, U extends any> = (WITHOUT<T> & U) | (WITHOUT<U> & T);

/**
 * @see https://github.com/microsoft/TypeScript/issues/14094#issuecomment-723571692
 */
export type ALLXOR<T extends Array<any>> = T extends [infer Only] ? Only : T extends [infer A, infer B, ...infer Rest] ? ALLXOR<[XOR<A, B>, ...Rest]> : never;

export type DockerOptions = { [option: string]: string };
export type DockerLabels = { [label: string]: string };

export type ObjectVersion = {
    Index: number;
};

export interface SwarmBase<IDType> {
    ID: IDType,
    Version: ObjectVersion,
    CreatedAt: string,
    UpdatedAt: string,
};

export type MountType = "bind" | "volume" | "tmpfs" | "npipe" | "cluster";

interface DockerResouceBase {
    NanoCPUs?: number,
    MemoryBytes?: number,
}

export interface Limit extends DockerResouceBase {
    Pids?: number // 0 = unlimited
}

interface GenericResourcesSpec<ValueType> {
    Kind: string,
    Value: ValueType
}

type NamedResourceSpec = { NamedResourceSpec: GenericResourcesSpec<string> };
type DiscreteResourceSpec = { DiscreteResourceSpec: GenericResourcesSpec<number> };

export type GenericResource = XOR<NamedResourceSpec, DiscreteResourceSpec>;

export interface ResourceObject extends DockerResouceBase {
    GenericResources?: Array<GenericResource>
}

export type DockerResources = {
    Limits?: Limit,
    Reservations?: ResourceObject
};

export type Platform = {
    Architecture: string,
    OS: string
};

export type Driver = {
    Name: string,
    Options?: DockerOptions
};

export type NetworkAttachementConfig = {
    Target: string,
    Aliases?: Array<string>,
    DriverOpts?: DockerOptions
};

type PortType = "tcp" | "udp" | "sctp";
export type Port = {
    IP?: string,
    PrivatePort: number,
    PublicPort?: number,
    Type: PortType
};

type EndpointPortConfigPublishMode = "ingress" | "host";
export type EndpointPortConfig = {
    Name?: string,
    Protocol: PortType,
    TargetPort: number,
    PublishedPort: number,
    PublishMode: EndpointPortConfigPublishMode
};

export type PortStatus = {
    Ports: Array<EndpointPortConfig>   
};

type EndpointSpecMode = "vip" | "dnsrr";
export type EndpointSpec = {
    Mode: EndpointSpecMode,
    Ports: Array<EndpointPortConfig>
};

export type PluginPrivilege = {
    Name: string,
    Description: string,
    Value: Array<string>
};

type MountBindOptionsPropagation = "private" | "rprivate" | "shared" | "rshared" | "slave" | "rslave";
type MountBindOptions = {
    Propagation: MountBindOptionsPropagation,
    NonRecursive: boolean, // default false
    CreateMountpoint: boolean, // default false
    ReadOnlyNonRecursive: boolean, // default false
    ReadOnlyForceRecursive: boolean // default false
};

type MountVolumeOptionsDriverConfig = {
    Name: string,
    Options: DockerOptions
};

type MountVolumeOptions = {
    NoCopy: boolean, // default false
    Labels: DockerLabels,
    DriverConfig: MountVolumeOptionsDriverConfig
};

type MountTmpfsOptions = {
    SizeBytes: number,
    Mode: number
};

type MountConsistency = "default" | "consistent" | "cached" | "delegated";
export type Mount = {
    Target: string,
    Source: string,
    Type: MountType,
    ReadOnly: boolean,
    Consistency: MountConsistency,
    BindOptions?: MountBindOptions,
    VolumeOptions?: MountVolumeOptions,
    TmpfsOptions?: MountTmpfsOptions
};

export type MountPoint = {
    Type: MountType,
    Name: string,
    Source: string,
    Destination: string,
    Driver: string,
    Mode: string,
    RW: boolean,
    Propagation: string
};

export type HealthCheckResult = {
    Start: string,
    End: string,
    ExitCode: number,
    Output: string
};

type HealthStatus = "none" | "starting" | "healthy" | "unhealthy";
export type Health = {
    Status: HealthStatus,
    FailingStreak: number,
    Log: Array<HealthCheckResult>
};

/**
 * @see https://docs.docker.com/engine/api/v1.44/#tag/Service/operation/ServiceList
 */
export type HealthConfig = {
    Test: Array<string>,
    Interval: number,
    Timeout: number,
    Retries: number,
    StartPeriod: number,
    StartInterval: number
};

type IPAMConfigAuxiliaryAdresses = {
    [name: string]: string
};

export type IPAMConfig = {
    Subnet: string,
    IPRange: string,
    Gateway: string,
    AuxiliaryAdresses?: IPAMConfigAuxiliaryAdresses
};

export type IPAM = {
    Driver: string,
    Config: Array<IPAMConfig> | null,
    Options: DockerOptions
};

type EndpointIPAMConfig = {
    IPv4Address: string,
    IPv6Address: string,
    LinkLocalIPs: Array<string>
};

export type EndpointSettings = {
    IPAMConfig: EndpointIPAMConfig | null,
    Links: Array<string>,
    MacAddress: string,
    Aliases: Array<string>,
    NetworkID: string,
    EndpointID: string,
    Gateway: string,
    IPAddress: string,
    IPPrefixLen: number,
    IPv6Gateway: string,
    GlobalIPv6Address: string,
    GlobalIPv6PrefixLen: number,
    DriverOpts: DockerOptions,
    DNSNames: Array<string>
};

export type Topology = {
    [name: string]: string
};

type EngineDescriptionPlugin = {
    Type: string,
    Name: string
};

export type EngineDescription = {
    EngineVersion: string,
    Labels: DockerLabels,
    Plugins: Array<EngineDescriptionPlugin>
};

export type TLSInfo = {
    TrustRoot: string,
    CertIssuerSubject: string,
    CertIssuerPublicKey: string
};

type RuntimeStatus = {
    [name: string]: string
};

export type Runtime = {
    path: string,
    runtimeArgs: Array<string> | null,
    status: RuntimeStatus
};

export type PeerNode = {
    NodeID: string,
    Addr: string
};

export type Isolation = "default" | "process" | "hyperv";

export type Commit = {
    ID: string,
    Expected: string
};

type BuildCacheType = "internal" | "frontend" | "source.local" | "source.git.checkout" | "exec.cachemount" | "regular";
export type BuildCache = {
    ID: string,
    Parent: string | null,
    Parents: Array<string> | null,
    Type: BuildCacheType,
    Description: string,
    InUse: boolean,
    Shared: boolean,
    Size: number,
    CreatedAt: string,
    LastUsedAt: string | null,
    UsageCount: number
};

type HostConfigBlkioWeightDevice = {
    Path: string,
    Weight: number
};

export type ThrottleDevice = {
    Path: string,
    Rate: number
};

export type DeviceMapping = {
    PathOnHost: string,
    PathInContainer: string,
    CgroupPermissions: string
};

export type DeviceRequest = {
    Driver: string,
    Count: number,
    DeviceIDs: Array<string>,
    Capabilities: Array<string>,
    Options: DockerOptions
};

type HostConfigUlimit = {
    Name: string,
    Soft: number,
    Hard: number
};

type LogConfigType = "json-file" | "syslog" | "journald" | "gelf" | "fluentd" | "awslogs" | "splunk" | "etwlogs" | "none";
export type LogConfig = {
    Type: LogConfigType,
    Config: DockerOptions
};

type PortBinding = {
    HostIp: string,
    HostPort: string
};

export type PortMap = {
    [name: string]: PortBinding
};

type RestartPolicyName = "" | "no" | "always" | "unless-stopped" | "on-failure";
export type RestartPolicy = {
    Name: RestartPolicyName,
    MaximumRetryCount: number
};

type HostConfigAnnotations = {
    [name: string]: string
};

type HostConfigCGroupNSMode = "private" | "host";
type HostConfigIpcMode = "none" | "private" | "shareable" | "host"; // also container:<name|id>

export type HostConfigBase = {
    CpuShares: number,
    Memory: number,
    CgroupParent: string,
    BlkioWeight: number,
    BlkioWeightDevice: Array<HostConfigBlkioWeightDevice>,
    BlkioDeviceReadBps: Array<ThrottleDevice>,
    BlkioDeviceWriteBps: Array<ThrottleDevice>,
    BlkioDeviceReadIOps: Array<ThrottleDevice>,
    BlkioDeviceWriteIOps: Array<ThrottleDevice>,
    CpuPeriod: number,
    CpuQuota: number,
    CpuRealtimePeriod: number,
    CpuRealtimeRuntime: number,
    CpusetCpus: string,
    CpusetMems: string,
    Devices: Array<DeviceMapping>,
    DeviceCgroupRules: Array<string>,
    DeviceRequests: Array<DeviceRequest>,
    KernelMemoryTCP: number,
    MemoryReservation: number,
    MemorySwap: number,
    MemorySwappiness: number,
    NanoCpus: number,
    OomKillDisable: boolean,
    Init: boolean | null,
    PidsLimit: number | null,
    Ulimits: Array<HostConfigUlimit>,
    CpuCount: number,
    CpuPercent: number,
    IOMaximumIOps: number,
    IOMaximumBandwidth: number
}

export type HostConfig = HostConfigBase & {
    Binds: Array<string>,
    ContainerIDFile: string,
    LogConfig: LogConfig,
    NetworkMode: string,
    PortBindings: PortMap,
    RestartPolicy: RestartPolicy,
    AutoRemove: boolean,
    VolumeDriver: string,
    VolumesFrom: Array<string>,
    Mounts: Array<Mount>,
    ConsoleSize: Array<number> | null,
    Annotations: HostConfigAnnotations,
    CapAdd: Array<string>,
    CapDrop: Array<string>,
    CgroupnsMode: HostConfigCGroupNSMode,
    Dns: Array<string>,
    DnsOptions: Array<string>,
    DnsSearch: Array<string>,
    ExtraHosts: Array<string>,
    GroupAdd: Array<string>,
    IpcMode: HostConfigIpcMode,
    Cgroup: string,
    Links: Array<string>,
    OomScoreAdj: number,
    PidMode: string,
    Privileged: boolean,
    PublishAllPorts: boolean,
    ReadonlyRootfs: boolean,
    SecurityOpt: Array<string>,
    StorageOpt: DockerOptions,
    Tmpfs: DockerOptions,
    UTSMode: string,
    UsernsMode: string,
    ShmSize: number,
    Sysctls: DockerOptions,
    Runtime: string,
    Isolation: Isolation,
    MaskedPaths: Array<string>,
    ReadonlyPaths: Array<string>
};

export type GraphDriverData = {
    Name: string,
    Data: DockerOptions
};

export type Address = {
    Addr: string,
    PrefixLen: number
};

export type DockerEventType = "builder" | "config" | "container" | "deamon" | "image" | "network" | "node" | "plugin" | "secret" | "service" | "volume";
export type DockerEventScope = "local" | "swarm";
export type DockerEventActorAttributes = { [Name: string]: string };

export type DockerEvent = {
    Type: DockerEventType,
    Action: string,
    Actor: {
        ID: string,
        Attributes: DockerEventActorAttributes
    },
    scope: DockerEventScope,
    time: number,
    timeNano: number
};