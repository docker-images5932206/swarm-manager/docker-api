import Modem, { ModemDialOptions } from "../api/Modem";
import { DockerLabels, DockerOptions, Driver, SwarmBase, TLSInfo } from "../utils/types";

type SwarmSpecOrchestration = {
    TaskHistoryRetentionLimit: number
};

type SwarmSpecRaft = {
    SnapshotInterval: number,
    KeepOldSnapshots: number,
    LogEntriesForSlowFollowers: number,
    ElectionTick: number,
    HeartbeatTick: number
};

type SwarmSpecDispatcher = {
    HeartbeatPeriod: number
};

type SwamSpecCAConfigExternalCAProtocol = "cfssl";
type SwamSpecCAConfigExternalCA = {
    Protocol: SwamSpecCAConfigExternalCAProtocol,
    URL: string,
    Options: DockerOptions,
    CACert: string
};

type SwarmSpecCAConfig = {
    NodeCertExpiry: number,
    ExternalCAs: Array<SwamSpecCAConfigExternalCA>,
    SigningCACert: string,
    SigningCAKey: string,
    ForceRotate: number
};

type SwarmSpecEncryptionConfig = {
    AutoLockManagers: boolean
};

type SwarmSpecTaskDefaults = {
    LogDriver: Driver
};

type SwarmSpec = {
    Name: string,
    Labels: DockerLabels,
    Orchestration: SwarmSpecOrchestration | null,
    Raft: SwarmSpecRaft,
    Dispatcher: SwarmSpecDispatcher,
    CAConfig: SwarmSpecCAConfig,
    EncryptionConfig: SwarmSpecEncryptionConfig,
    TaskDefaults: SwarmSpecTaskDefaults
};

export interface SwarmCluster extends SwarmBase<string> {
    Spec: SwarmSpec,
    TLSInfo: TLSInfo,
    RootRotationInProgress: boolean,
    DataPathPort: number,
    DefaultAddrPool: Array<Array<string>>,
    SubnetSize: number
}

type SwarmInitSpecBase = {
    ListenAddr: string,
    AdvertiseAddr: string,
    DataPathAddr?: string
};

export type SwarmInitSpec = SwarmInitSpecBase & {
    DataPathPort: number,
    DefaultAddrPool: Array<string>,
    ForceNewCluster: boolean,
    SubnetSize: number,
    Spec: SwarmSpec
};

export type SwarmJoinSpec = SwarmInitSpecBase & {
    RemoteAddrs: Array<string>,
    JoinToken: string
};

export type SwarmLeaveQueryOptions = {
    force?: boolean
};

export type SwarmUpdateQueryOptions = {
    rotateWorkerToken?: boolean,
    rotateManagerToken?: boolean,
    rotateManagerUnlockKey?: boolean
};

export type SwarmUnlockSpec = {
    UnlockKey: string
};

export default class Swarm {
    static async inspect(modem: Modem) {
        const call: ModemDialOptions = {
            path: "/swarm",
            method: "GET"
        };

        return await modem.dial(call) as SwarmCluster;
    }

    static async initialize(modem: Modem, initSpec: SwarmInitSpec) {
        const call: ModemDialOptions = {
            path: "/swarm/init",
            method: "POST",
            body: initSpec
        };

        return await modem.dial(call);
    }
    
    static async join(modem: Modem, joinSpec: SwarmJoinSpec) {
        const call: ModemDialOptions = {
            path: "/swarm/join",
            method: "POST",
            body: joinSpec
        };

        return await modem.dial(call);
    }

    static async leave(modem: Modem, queryOptions: SwarmLeaveQueryOptions = {}) {
        const query = new Map();
        query.set("force", queryOptions.force ? "true" : "false");

        const call: ModemDialOptions = {
            path: "/swarm/leave",
            method: "POST",
            query: query
        };

        return await modem.dial(call);
    }

    static async update(modem: Modem, queryOptions: SwarmUpdateQueryOptions = {}, spec: SwarmSpec) {
        const current = await Swarm.inspect(modem);
        const version = current.Version.Index;

        const query = new Map();
        query.set("version", version);
        query.set("rotateWorkerToken", queryOptions.rotateWorkerToken ? "true" : "false");
        query.set("rotateManagerToken", queryOptions.rotateManagerToken ? "true" : "false");
        query.set("rotateManagerUnlockKey", queryOptions.rotateManagerUnlockKey ? "true" : "false");

        const call: ModemDialOptions = {
            path: "/swarm/update",
            method: "POST",
            query: query,
            body: spec
        };

        return await modem.dial(call);
    }

    static async getUnlockKey(modem: Modem) {
        const call: ModemDialOptions = {
            path: "/swarm/unlockkey",
            method: "GET"
        };

        return await modem.dial(call);
    }

    static async unlock(modem: Modem, unlockSpec: SwarmUnlockSpec) {
        const call: ModemDialOptions = {
            path: "/swarm/unlock",
            method: "POST",
            body: unlockSpec
        };

        return await modem.dial(call);
    }
}