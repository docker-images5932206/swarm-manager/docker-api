import Modem, { ModemDialOptions } from "../api/Modem";
import ContainerHandle, { Container, ContainerId, ContainerSpec, ContainerSummary } from "./container";

type ContainerListFilterType = "ancestor" | "before" | "expose" | "exited" | "isolation" | "is-task" | "label" | "name" | "network" | "publish" | "since" | "status" | "volume";
type ContainerListFilters = Map<ContainerListFilterType, Array<string>>;

type ContainerPruneFilterType = "until" | "label";
type ContainerPruneFilters = Map<ContainerPruneFilterType, Array<string>>;

export type ContainerListQueryOptions = {
    all?: boolean,
    limit?: number,
    size?: boolean,
    filters?: ContainerListFilters
};

export type ContainerPruneQueryOptions = {
    filters?: ContainerPruneFilters
};

export type ContainerListResult = Array<ContainerSummary>;
type ContainerCreateResult = {
    Id: string,
    Warnings: Array<string>
};
type ContainerPruneResult = {
    ContainersDeleted: Array<string>,
    SpaceReclaimed: number
};

export default class Containers {
    static async list(modem: Modem, queryOptions: ContainerListQueryOptions = {}) {
        const query = new Map();
        if (queryOptions.all) query.set("all", queryOptions.all ? "true" : "false");
        if (queryOptions.size) query.set("size", queryOptions.size ? "true" : "false");
        if (queryOptions.limit) query.set("limit", queryOptions.limit);
        if (queryOptions.filters) query.set("filters", JSON.stringify(Object.fromEntries(queryOptions.filters)));

        const call: ModemDialOptions = {
            path: "/containers/json",
            method: "GET",
            query: query
        };

        return await modem.dial(call) as ContainerListResult;
    }

    static async create(modem: Modem, spec: ContainerSpec) {
        const call: ModemDialOptions = {
            path: "/containers/create",
            method: "POST",
            body: spec
        };

        const result = await modem.dial(call) as ContainerCreateResult;
        return new ContainerHandle(modem, result.Id);
    }

    static async prune(modem: Modem, queryOptions: ContainerPruneQueryOptions = {}) {
        const query = new Map();
        if (queryOptions.filters) query.set("filters", JSON.stringify(Object.fromEntries(queryOptions.filters)));

        const call: ModemDialOptions = {
            path: "/containers/prune",
            method: "POST",
            query: query
        };

        return await modem.dial(call) as ContainerPruneResult;
    }

    static getHandle(modem: Modem, id: ContainerId) {
        return new ContainerHandle(modem, id);
    }
}