import Modem, { ModemDialOptions, ModemDialResult, ModemDialStreamResult } from "../api/Modem";
import Handle from "../utils/Handle";
import { DockerLabels, Port, EndpointSettings, MountPoint, HealthConfig, Health, HostConfig, GraphDriverData, PortMap, Address, HostConfigBase, RestartPolicy } from "../utils/types";

export type ContainerId = string;

type ContainerHostConfig = {
    NetworkMode?: string
};

type ContainerSummaryNetworkSettings = {
    Networks: {
        [name: string]: EndpointSettings
    }
};

interface ContainerBase {
    Id: ContainerId,
    Image: string,
    Mounts: Array<MountPoint>
}

export interface ContainerSummary extends ContainerBase {
    Names: Array<string>,
    ImageID: string,
    Command: string,
    Created: number,
    Ports: Array<Port>,
    SizeRw: number,
    SizeRootFs: number,
    Labels: DockerLabels,
    State: string,
    Status: string,
    HostConfig: ContainerHostConfig,
    NetworkSettings: ContainerSummaryNetworkSettings,
}

export type ContainerStatus = {
    ContainerID: ContainerId,
    PID: string,
    ExitCode: number
};

/**
 * Format: <port>/<tcp|udp|sctp>: {}
 */
type ContainerSpecExposedPorts = {
    [port: string]: object
};

type ContainerVolumes = {
    [name: string]: object
};

type ContainerSpecNetworkingConfig = {
    EndpointsConfig: EndpointSettings
};

export type ContainerConfig = {
    Hostname?: string,
    Domainname?: string,
    User?: string,
    AttachStdin?: boolean,
    AttachStdout?: boolean,
    AttachStderr?: boolean,
    ExposedPorts?: ContainerSpecExposedPorts,
    Tty?: boolean,
    OpenStdin?: boolean,
    StdinOnce?: boolean,
    Env?: Array<string>,
    Cmd?: Array<string>,
    Healthcheck?: HealthConfig,
    ArgsEscaped?: boolean | null,
    Image: string,
    Volumes?: ContainerVolumes,
    WorkingDir?: string,
    Entrypoint?: Array<string>,
    NetworkDisabled?: boolean | null,
    MacAddress?: string | null,
    OnBuild?: Array<string> | null,
    Labels?: DockerLabels,
    StopSignal?: string | null,
    StopTimeout?: number | null,
    Shell?: Array<string> | null,
}

export type ContainerSpec = ContainerConfig & {
    HostConfig?: ContainerHostConfig,
    NetworkingConfig?: ContainerSpecNetworkingConfig
};

type ContainerStateStatus = "created" | "running" | "paused" | "restarting" | "removing" | "exited" | "dead";
type ContainerState = {
    Status: ContainerStateStatus,
    Running: boolean,
    Paused: boolean,
    Restarting: boolean,
    OOMKilled: boolean,
    Dead: boolean,
    Pid: number,
    ExitCode: number,
    Error: string,
    StartedAt: string,
    FinishedAt: string,
    Health: Health
};

type ContainerNetworkSettings = ContainerSummaryNetworkSettings & {
    Bridge: string,
    SandboxID: string,
    HairpinMode: boolean,
    LinkLocalIPv6Address: string,
    LinkLocalIPv6PrefixLen: number,
    Ports: PortMap,
    SandboxKey: string,
    SecondaryIPAddresses: Array<Address> | null,
    SecondaryIPv6Addresses: Array<Address> | null
}

export interface Container extends ContainerBase {
    Created: string,
    Path: string,
    Args: Array<string>,
    State: ContainerState,
    ResolvConfPath: string,
    HostnamePath: string,
    HostsPath: string,
    LogPath: string,
    Name: string,
    RestartCount: number,
    Driver: string,
    Platform: string,
    MountLabel: string,
    ProcessLabel: string,
    AppArmorProfile: string,
    ExecIds: Array<string> | null,
    HostConfig: HostConfig,
    GraphDriver: GraphDriverData,
    SizeRw: number,
    SizeRootFs: number,
    Mounts: Array<MountPoint>,
    Config: ContainerConfig,
    NetworkSettings: ContainerNetworkSettings
};

export type ContainerUpdateConfig = HostConfigBase & {
    RestartPolicy: RestartPolicy
};

export type ContainerInspectQueryOptions = {
    size?: boolean
};

export type ContainerListProcessesQueryOptions = {
    ps_args?: string
};

export type ContainerLogsQueryOptions = {
    follow?: boolean,
    stdout?: boolean,
    stderr?: boolean,
    since?: number,
    until?: number,
    timestamps?: boolean,
    tail?: "all" | number
}

export type ContainerListProcessesResult = {
    Titles: Array<string>,
    Processes: Array<Array<string>>
};

export type ContainerChangesResult = Array<{
    Path: string,
    Kind: 0 | 1 | 2
}>;

export type ContainerStatsQueryOptions = {
    stream?: boolean,

    /**
     * Has to be used with stream=false  
     * Returns values immediately instead of waiting 2 cycles
     */
    oneShot?: boolean
};

export type ContainerResizeQueryOptions = {
    w: number,
    h: number
};

export type ContainerStartQueryOptions = {
    detachKeys?: string
};

export type ContainerKillQueryOptions = {
    signal?: string,
}

export type ContainerStopQueryOptions = ContainerKillQueryOptions & {
    t?: number
};

export type ContainerRenameQueryOptions = {
    name: string
};

export type ContainerWaitQueryOptions = {
    condition?: "not-running" | "next-exit" | "removed"
};

export type ContainerRemoveQueryOptions = {
    v?: boolean,
    force?: boolean,
    link?: boolean
};

export type ContainerUpdateResult = {
    Warnings: Array<string>
};

export default class ContainerHandle extends Handle {
    async inspect(queryOptions: ContainerInspectQueryOptions = {}) {
        const query = new Map();
        if ("size" in queryOptions) query.set("size", queryOptions.size ? "true" : "false");

        const call: ModemDialOptions = {
            path: "/containers/" + this.id + "/json",
            method: "GET",
            query: query
        };

        return await this.modem.dial(call) as Container;
    }

    async listProcesses(queryOptions: ContainerListProcessesQueryOptions = {}) {
        const query = new Map();
        query.set("ps_args", queryOptions.ps_args ?? "-ef");

        const call: ModemDialOptions = {
            path: `/containers/${this.id}/top`,
            method: "GET",
            query: query
        };

        return await this.modem.dial(call) as ContainerListProcessesResult;
    }

    public async getLogs(queryOptions: ContainerLogsQueryOptions = {}) {
        const query = new Map();
        query.set("follow", queryOptions.follow ? "true" : "false");
        query.set("stdout", queryOptions.stdout ? "true" : "false");
        query.set("stderr", queryOptions.stderr ? "true" : "false");
        query.set("since", queryOptions.since ?? 0);
        query.set("until", queryOptions.until ?? 0);
        query.set("timestamps", queryOptions.timestamps ? "true" : "false");
        query.set("tail", queryOptions.tail ?? "all");

        const call : ModemDialOptions = {
            path: `/containers/${this.id}/logs`,
            method: "GET",
            query: query
        };

        return await this.modem.dial(call) as ModemDialStreamResult;
    }

    async changes() {
        const call: ModemDialOptions = {
            path: `/containers/${this.id}/changes`,
            method: "GET",
        };

        return await this.modem.dial(call) as ContainerChangesResult;
    }

    async export() {
        const call: ModemDialOptions = {
            path: `/containers/${this.id}/export`,
            method: "GET",
            isStream: true
        };

        return await this.modem.dial(call) as ModemDialStreamResult;
    }

    async stats(queryOptions: ContainerStatsQueryOptions = {}) {
        const query = new Map();
        if ("stream" in queryOptions) query.set("stream", queryOptions.stream ? "true" : "false");
        if ("oneShot" in queryOptions) query.set("one-shot", queryOptions.oneShot ? "true" : "false");

        const call: ModemDialOptions = {
            path: "/containers/" + this.id + "/stats",
            method: "GET",
            query: query,
            isStream: queryOptions.stream
        };

        return await this.modem.dial(call) as ModemDialStreamResult;
    }

    async resize(queryOptions: ContainerResizeQueryOptions) {
        const query = new Map();
        query.set("h", queryOptions.h);
        query.set("w", queryOptions.w);

        const call: ModemDialOptions = {
            path: `/containers/${this.id}/resize`,
            method: "POST",
            query: query
        };

        return await this.modem.dial(call);
    }

    async start(queryOptions: ContainerStartQueryOptions = {}) {
        const query = new Map();
        if (queryOptions.detachKeys) query.set("detachKeys", queryOptions.detachKeys);

        const call: ModemDialOptions = {
            path: `/containers/${this.id}/start`,
            method: "POST",
            query: query
        };

        return await this.modem.dial(call);
    }

    async stop(queryOptions: ContainerStopQueryOptions = {}) {
        const query = new Map();
        query.set("signal", queryOptions.signal ?? "SIGINT");
        query.set("t", typeof queryOptions.t === "undefined" ? 5 : queryOptions.t);

        const call: ModemDialOptions = {
            path: `/containers/${this.id}/stop`,
            method: "POST",
            query: query
        };

        return await this.modem.dial(call);
    }

    async restart(queryOptions: ContainerStopQueryOptions = {}) {
        const query = new Map();
        query.set("signal", queryOptions.signal ?? "SIGINT");
        query.set("t", queryOptions.t);

        const call: ModemDialOptions = {
            path: `/containers/${this.id}/restart`,
            method: "POST",
            query: query
        };

        return await this.modem.dial(call);
    }

    async kill(queryOptions: ContainerKillQueryOptions = {}) {
        const query = new Map();
        query.set("signal", queryOptions.signal ?? "SIGKILL");

        const call: ModemDialOptions = {
            path: `/containers/${this.id}/kill`,
            method: "POST",
            query: query
        };

        return await this.modem.dial(call);
    }

    public async update(updateConfig: ContainerUpdateConfig) {
        const call: ModemDialOptions = {
            path: `/containers/${this.id}/update`,
            method: "POST",
            body: updateConfig
        };

        return await this.modem.dial(call) as ContainerUpdateResult;
    }

    async rename(queryOptions: ContainerRenameQueryOptions) {
        const query = new Map();
        query.set("name", queryOptions.name);

        const call: ModemDialOptions = {
            path: `/containers/${this.id}/rename`,
            method: "POST",
            query: query
        };

        return await this.modem.dial(call);
    }

    async pause() {
        const call: ModemDialOptions = {
            path: `/containers/${this.id}/pause`,
            method: "GET",
            isStream: true
        };

        return await this.modem.dial(call);
    }

    async unpause() {
        const call: ModemDialOptions = {
            path: `/containers/${this.id}/unpause`,
            method: "GET",
            isStream: true
        };

        return await this.modem.dial(call);
    }

    // TODO attach to container
    // TODO attach to container via websocket

    async wait(queryOptions: ContainerWaitQueryOptions = {}) {
        const query = new Map();
        query.set("condition", queryOptions.condition ?? "not-running");

        const call: ModemDialOptions = {
            path: `/containers/${this.id}/wait`,
            method: "POST",
            query: query
        };

        return await this.modem.dial(call);
    }

    async remove(queryOptions: ContainerRemoveQueryOptions = {}) {
        const query = new Map();
        query.set("v", queryOptions.v ? "true" : "false");
        query.set("force", queryOptions.force ? "true" : "false");
        query.set("link", queryOptions.link ? "true" : "false");

        const call: ModemDialOptions = {
            path: "/containers/" + this.id,
            method: "DELETE",
            query: query
        };

        await this.modem.dial(call);
    }

    // TODO implement archive endpoints

    static fromContainer(modem: Modem, container: ContainerSummary) {
        return new ContainerHandle(modem, container.Id);
    }
}