import Modem, { ModemDialOptions } from "../api/Modem";
import Handle from "../utils/Handle";

export type PluginId = string;

type PluginMount = {
    Name: string,
    Description: string,
    Settable: Array<string>,
    Source: string,
    Destination: string,
    Type: string,
    Options: Array<string>
};

type PluginDevice = {
    Name: string,
    Description: string,
    Settable: Array<string>,
    Path: string
};

type PluginSettings = {
    Mounts: Array<PluginMount>,
    Env: Array<string>,
    Args: Array<string>,
    Devices: Array<PluginDevice>
};

type PluginInterfaceType = {
    Prefix: string,
    Capability: string,
    Version: string
};

type PluginInterfaceProtocolScheme = "" | "moby.plugins.http/v1";
type PluginInterface = {
    Types: Array<PluginInterfaceType>,
    Socket: string,
    ProtocolScheme: PluginInterfaceProtocolScheme
};

type PluginUser = {
    UID: number,
    GID: number
};

type PluginNetwork = {
    Type: string
};

type PluginLinux = {
    Capabilities: Array<string>,
    AllowAllDevices: boolean,
    Devices: Array<PluginDevice>
};

type PluginEnv = {
    Name: string,
    Description: string,
    Settable: Array<string>,
    Value: string
};

type PluginArgs = {
    Name: string,
    Description: string,
    Settable: Array<string>,
    Value: Array<string>
};

type PluginRootfs = {
    type: string,
    diff_ids: Array<string>
};

type PluginConfig = {
    Docker: string,
    Description: string,
    Documentation: string,
    Interface: PluginInterface,
    Entrypoint: Array<string>,
    WorkDir: string,
    User: PluginUser,
    Network: PluginNetwork,
    Linux: PluginLinux,
    PopagatedMount: string,
    IpcHost: boolean,
    PidHost: boolean,
    Mounts: Array<PluginMount>,
    Env: Array<PluginEnv>,
    Args: PluginArgs,
    rootfs: PluginRootfs
};

export interface Plugin {
    Id: PluginId,
    Name: string,
    Enabled: boolean,
    Settings: PluginSettings,
    PluginReference: string,
    Config: PluginConfig
};

export type PluginRemoveQueryOptions = {
    force?: boolean
};

export type PluginEnableQueryOptions = {
    timeout?: number
};

export type PluginDisableQueryOptions = {
    force?: boolean
};

export default class PluginHandle extends Handle {
    async inspect() {
        const call: ModemDialOptions = {
            path: "/plugins/" + this.id + "/json",
            method: "GET"
        };

        return await this.modem.dial(call) as Plugin;
    }

    async remove(queryOptions: PluginRemoveQueryOptions = {}) {
        const query = new Map();
        query.set("force", queryOptions.force ? "true" : "false");

        const call: ModemDialOptions = {
            path: "/plugins/" + this.id,
            method: "DELETE",
            query: query
        };

        await this.modem.dial(call);
    }

    async enable(queryOptions: PluginEnableQueryOptions = {}) {
        const query = new Map();
        query.set("timeout", queryOptions.timeout ? queryOptions.timeout : 0);

        const call: ModemDialOptions = {
            path: "/plugins/" + this.id + "/enable",
            method: "POST",
            query: query
        };

        await this.modem.dial(call);
    }

    async disable(queryOptions: PluginDisableQueryOptions = {}) {
        const query = new Map();
        query.set("force", queryOptions.force ? "true" : "false");

        const call: ModemDialOptions = {
            path: "/plugins/" + this.id + "/disable",
            method: "POST",
            query: query
        };

        await this.modem.dial(call);
    }

    static fromPlugin(modem: Modem, plugin: Plugin) {
        const id = plugin.Id;
        return new PluginHandle(modem, id);
    }
}