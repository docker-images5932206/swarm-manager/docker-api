import Modem, { ModemDialOptions, ModemDialStreamResult } from "../api/Modem";
import HandleLogWrapper from "../utils/HandleLogWrapper";
import { PluginPrivilege } from "../utils/types";
import PluginHandle, { PluginId, Plugin } from "./plugin";

type PluginListFilterType = "capability" | "enable";

export type PluginListFilters = Map<PluginListFilterType, Array<string>>;
export type PluginListResult = Array<Plugin>;

export type PluginListQueryOptions = {
    filters?: PluginListFilters
};

export type PluginGetPrivilegesQueryOptions = {
    remote: string
};

type PluginGetPrivilegesResult = Array<PluginPrivilege>;

export type PluginInstallQueryOptions = PluginGetPrivilegesQueryOptions & {
    name?: string
};

export default class Plugins {
    static async list(modem: Modem, queryOptions: PluginListQueryOptions = {}) {
        const query = new Map();
        if (queryOptions.filters) query.set("filter", JSON.stringify(Object.fromEntries(queryOptions.filters)));

        const call: ModemDialOptions = {
            path: "/plugins",
            method: "GET",
            query: query
        };

        return await modem.dial(call) as PluginListResult;
    }

    /**
     * 
     * @param modem modem to use
     * @param queryOptions query options to use
     * @param registryAuth is not included in docker api docs but is required for private repos to correctly return privileges
     * @returns 
     */
    static async getPrivileges(modem: Modem, queryOptions: PluginGetPrivilegesQueryOptions, registryAuth?: string) {
        const headers = new Map();
        if (registryAuth) headers.set("X-Registry-Auth", registryAuth);

        const query = new Map();
        query.set("remote", queryOptions.remote);

        const call: ModemDialOptions = {
            path: "/plugins/privileges",
            method: "GET",
            headers: headers,
            query: query
        };

        return await modem.dial(call) as PluginGetPrivilegesResult;
    }

    static async install(modem: Modem, queryOptions: PluginInstallQueryOptions, privileges: Array<PluginPrivilege>, registryAuth?: string) {
        const headers = new Map();
        if (registryAuth) headers.set("X-Registry-Auth", registryAuth);

        const query = new Map();
        query.set("remote", queryOptions.remote);
        if (queryOptions.name) query.set("name", queryOptions.name);

        const call: ModemDialOptions = {
            path: "/plugins/pull",
            method: "POST",
            headers: headers,
            query: query,
            body: privileges,
            isStream: true // needed here because the endpoint streams the stdout of the plugin install process
        };

        const name = queryOptions.name ? queryOptions.name : queryOptions.remote;

        // TODO do do someting with the stdout from this endpoint
        // throw an error if it could not install
        const stream = await modem.dial(call) as ModemDialStreamResult;
        const handle = new PluginHandle(modem, name);
        const logWrapper = new HandleLogWrapper(handle, stream);

        await logWrapper.drainLogStream();
        return logWrapper;
    }

    static async installGrantAllPrivileges(modem: Modem, queryOptions: PluginInstallQueryOptions, registryAuth?: string) {
        const privileges = await Plugins.getPrivileges(modem, queryOptions as PluginGetPrivilegesQueryOptions, registryAuth);
        return await Plugins.install(modem, queryOptions, privileges, registryAuth);
    }

    static getHandle(modem: Modem, id: PluginId) {
        return new PluginHandle(modem, id);
    }
}

// IDEA maybe add creating plugins
// IDEA maybe add upgrading plugins
// IDEA maybe add pushing plugins
// IDEA maybe add configuring plugins