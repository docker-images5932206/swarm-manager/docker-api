import Modem, { ModemDialOptions, ModemDialStreamResult } from "../api/Modem";
import { ContainerSummary } from "../container/container";
import { ImageSummary } from "../image/image";
import { SwarmCluster } from "../swarm/swarm";
import { DockerOptions, DockerLabels, GenericResource, ObjectVersion, PeerNode, Runtime, Driver, TLSInfo, Isolation, Commit, BuildCache } from "../utils/types";
import { ClusterVolume } from "../volume/clusterVolume";
import { Volume } from "../volume/volume";

export interface AuthCredentials {
    username: string,
    password: string,
    email: string,
    serveraddress: string
}

type AuthResult = {
    Status: string,
    IdentityToken: string
};

type SystemPluginsInfo = {
    Volume: Array<string>,
    Network: Array<string>,
    Authroization: Array<string>,
    Log: Array<string>
};

type SystemCgroupDriver = "cgroupfs" | "systemd" | "none";
type SystemCgroupVersion = "1" | "2";

type SystemRegistryServiceConfigIndexConfig = {
    [name: string]: object | null
};

type SystemRegistryServiceConfig = {
    AllowNondistributableArtifactsCIDRs: Array<string>,
    AllowNondistributableArtifactsHostnames: Array<string>,
    InsecureRegistryCIDRs: Array<string>,
    IndexConfig: SystemRegistryServiceConfigIndexConfig,
    Mirrors: Array<string>
};

type SystemRuntimes = {
    [name: string]: Runtime
};

type SystemSwarmInfoLocalNodeState = "" | "inactive" | "pending" | "active" | "error" | "locked";
type SystemSwarmInfo = {
    NodeID: string,
    NodeAddr: string,
    LocalNodeState: SystemSwarmInfoLocalNodeState,
    ControlAvailable: boolean,
    Error: string,
    RemoteManagers: Array<PeerNode> | null,
    Nodes: number | null,
    Managers: number | null,
    Cluster: SwarmCluster
};

type SystemDefaultAddressPool = {
    Base: string,
    Site: number
};

type SystemInformation = {
    ID: string,
    Containers: number,
    ContainersRunning: number,
    ContainersPaused: number,
    ContainersStopped: number,
    Images: number,
    Driver: string,
    DriverStatus: Array<Array<string>>,
    DockerRootDir: string,
    Plugins: SystemPluginsInfo,
    MemoryLimit: boolean,
    SwapLimit: boolean,
    KernelMemoryTCP: boolean,
    CpuCfsPeriod: boolean,
    CpuCfsQuota: boolean,
    CPUShares: boolean,
    CPUSet: boolean,
    PidsLimit: boolean,
    OomKillDisable: boolean,
    IPv4Forwarding: boolean,
    BridgeNfIptables: boolean,
    BridgeNfIp6tables: boolean,
    Debug: boolean,
    NFd: number,
    NGoroutines: number,
    SystemTime: string,
    LoggingDriver: string,
    CgroupDriver: SystemCgroupDriver,
    CgroupVersion: SystemCgroupVersion,
    NEventsListener: number,
    KernelVersion: string,
    OperatingSystem: string,
    OSVersion: string,
    OSType: string,
    Architecture: string,
    NCPU: number,
    MemTotal: number,
    IndexServerAddress: string,
    RegistryConfig: SystemRegistryServiceConfig,
    GenericResources: Array<GenericResource>,
    HttpProxy: string,
    HttpsProxy: string,
    NoProxy: string,
    Name: string,
    Labels: Array<string>,
    ExperimentalBuild: boolean,
    ServerVersion: string,
    Runtimes: SystemRuntimes,
    DefaultRuntime: string,
    Swarm: SystemSwarmInfo,
    LiveRestoreEnabled: boolean,
    Isolation: Isolation,
    InitBinary: string,
    ContainerdCommit: Commit,
    RuncCommit: Commit,
    InitCommit: Commit,
    SecurityOptions: Array<string>,
    ProductLicense: string,
    DefaultAddressPools: Array<SystemDefaultAddressPool>,
    Warnings: Array<string>,
    CDISpecDirs: Array<string>
};

type SystemVersionPlatform = {
    Name: string
};

type SystemVersionComponent = {
    Name: string,
    Version: string,
    Details: object | null
;}

export interface SystemVersion {
    Platform: SystemVersionPlatform,
    Components: Array<SystemVersionComponent>,
    Version: string,
    ApiVersion: string,
    MinAPIVersion: string,
    GitCommit: string,
    GoVersion: string,
    Os: string,
    Arch: string,
    KernelVersion: string,
    Experimental: boolean,
    BuildTime: string
};

type SystemPingDataSwarm = "inactive" | "pending" | "error" | "locked" | "active/worker" | "active/manager";
type SystemPingDataCacheControl = "no-cache" | "no-store" | "must-revalidate";
export interface SystemPingData {
    "API-Version": string,
    "Builder-Version": string,
    "Docker-Experimental": boolean,
    Swarm: SystemPingDataSwarm,
    "Cache-Control": SystemPingDataCacheControl,
    Pragma: string
}

export type SystemUsageDataType = "container" | "image" | "volume" | "build-cache";
export type SystemUsageDataQueryOptions = {
    type?: Array<SystemUsageDataType>
};

export type SystemMonitorEventsFilterType = "config" | "container" | "daemon" | "event" | "image" | "label" | "network" | "node" | "plugin" | "scope" | "secret" | "service" | "type" | "volume";
/**
 * Query options for minitoring events
 * @see https://docs.docker.com/reference/api/engine/version/v1.44/#tag/System/operation/SystemEvents
 */
export type SystemMonitorEventsQueryOptions = {
    since?: string,
    until?: string,
    filters?: Map<SystemMonitorEventsFilterType, Array<string>>
};

type SystemMonitorEventsActorAttributes = {
    [name: string]: string
};

type SystemMonitorEventsActor = {
    ID: string,
    Attributes: SystemMonitorEventsActorAttributes
};

export type SystemMonitorEventsResultType = "builder" | "config" | "container" | "daemon" | "image" | "network" | "node" | "plugin" | "secret" | "service" | "volume";
export type SystemMonitorEventsScope = "local" | "swarm";
export type SystemMonitorEventsResult = {
    Type: SystemMonitorEventsResultType,
    Action: string,
    Actor: SystemMonitorEventsActor,
    scope: SystemMonitorEventsScope,
    time: number,
    timeName: number
}

type SystemUsageData = {
    LayerSize: number,
    Images: Array<ImageSummary>,
    Containers: Array<ContainerSummary>,
    Volumes: Array<Volume | ClusterVolume>,
    BuildCache: Array<BuildCache>
};

export default class System {
    static async auth(modem: Modem, credentials: AuthCredentials) {
        const call: ModemDialOptions = {
            path: "/auth",
            method: "POST",
            body: credentials
        };

        return await modem.dial(call) as AuthResult;
    }

    static async info(modem: Modem) {
        const call: ModemDialOptions = {
            path: "/info",
            method: "GET"
        };

        return await modem.dial(call) as SystemInformation;
    }

    static async version(modem: Modem) {
        const call: ModemDialOptions = {
            path: "/version",
            method: "GET"
        };

        return await modem.dial(call) as SystemVersion;
    }

    static async ping(modem: Modem) {
        const call: ModemDialOptions = {
            path: "/_ping",
            method: "GET"
        };

        await modem.dial(call) as SystemPingData;
    }

    /**
     * @see https://docs.docker.com/engine/api/v1.44/#tag/System/operation/SystemEvents
     * 
     * @param modem 
     * @param queryOptions 
     */
    static async monitorEvents(modem: Modem, queryOptions: SystemMonitorEventsQueryOptions = {}) {
        const query = new Map();
        if (queryOptions.since) query.set("since", queryOptions.since);
        if (queryOptions.until) query.set("until", queryOptions.until);
        if (queryOptions.filters) query.set("filters", JSON.stringify(Object.fromEntries(queryOptions.filters)));
        
        const call: ModemDialOptions = {
            path: "/events",
            method: "GET",
            query: query,
            isStream: true
        };

        return await modem.dial(call) as ModemDialStreamResult;
    }

    static async usageData(modem: Modem, queryOptions: SystemUsageDataQueryOptions = {}) {
        const query = new Map();
        if (queryOptions.type) query.set("type", JSON.stringify(queryOptions.type));

        const call: ModemDialOptions = {
            path: "/system/df",
            method: "GET",
            query: query
        };

        return await modem.dial(call) as SystemUsageData;
    }
}