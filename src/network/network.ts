import Modem, { ModemDialOptions } from "../api/Modem";
import { DOCKER_STACK_NAMESPACE_LABEL } from "../stack/stackDeps";
import Handle from "../utils/Handle";
import Stackable from "../utils/Stackable";
import { DockerOptions, DockerLabels, IPAM, EndpointSpec, EndpointSettings } from "../utils/types";

export type NetworkId = string;
type NetworkScope = "local" | "global" | "swarm";

export type NetworkContainer = {
    Name: string,
    EndpointID: string,
    MacAddress: string,
    IPv4Address: string,
    IPv6Address: string
};

type NetworkContainers = {
    [name: string]: NetworkContainer
};

type NetworkConfigFrom = {
    Network: NetworkId
};

export interface NetworkSpec {
    Name: string,
    Driver?: string,
    EnableIPv6?: boolean,
    IPAM?: IPAM,
    Internal?: boolean,
    Attachable?: boolean,
    Ingress?: boolean,
    Options?: DockerOptions,
    Labels?: DockerLabels,
    Scope?: NetworkScope,
    ConfigOnly?: boolean,
    ConfigFrom?: NetworkConfigFrom
}

export interface Network extends NetworkSpec {
    Id: NetworkId,
    Created: string,
    Containers: NetworkContainers,
}

export type NetworkInspectQueryOptions = {
    verbose?: boolean,
    scope?: NetworkScope
};

export type NetworkConnectContainerConfig = {
    Container: string,
    EndpointConfig: EndpointSettings
};

export type NetworkDisconnectContainerConfig = {
    Container: string,
    Force: boolean
};

export default class NetworkHandle extends Handle implements Stackable {
    /**
     * @see https://docs.docker.com/engine/api/v1.44/#tag/Network/operation/NetworkInspect
     * @param queryOptions query options to use 
     */
    public async inspect(queryOptions: NetworkInspectQueryOptions = {}) {
        const query = new Map();
        query.set("verbose", queryOptions.verbose ? "true" : "false");
        if (queryOptions.scope) query.set("scope", queryOptions.scope);

        const call: ModemDialOptions = {
            path: "/networks/" + this.id,
            method: "GET",
            query: query
        };

        return await this.modem.dial(call) as Network;
    }

    public async remove() {
        const current = await this.inspect();

        if (this.shouldThrowStackError() && NetworkHandle.isPartOfStack(current))
            throw new Error("The current network was created by a stack, it can not be removed individually");

        const call: ModemDialOptions = {
            path: "/networks/" + this.id,
            method: "DELETE"
        };

        await this.modem.dial(call);
    }

    public async connectContainer(connectConfig: NetworkConnectContainerConfig) {
        const call: ModemDialOptions = {
            path: `/networks/${this.id}/connect`,
            method: "POST",
            body: connectConfig
        };

        return await this.modem.dial(call);
    }

    public async disconnectContainer(disconnectConfig: NetworkDisconnectContainerConfig) {
        const call: ModemDialOptions = {
            path: `/networks/${this.id}/disconnect`,
            method: "POST",
            body: disconnectConfig
        };

        return await this.modem.dial(call);
    }

    public shouldThrowStackError() {
        return true;
    }

    public static isPartOfStack(network: Network) {
        return network.Labels && DOCKER_STACK_NAMESPACE_LABEL in network.Labels;
    }

    public static fromNetwork(modem: Modem, network: Network) {
        const id = network.Id;
        return new NetworkHandle(modem, id);
    }
}