import Modem, { ModemDialOptions } from "../api/Modem";
import NetworkHandle, { Network, NetworkId, NetworkSpec } from "./network";

type NetworkListFilterType = "dangling" | "driver" | "id" | "label" | "name" | "scope" | "type";
type NetworkPruneFilterType = "until" | "label";

export type NetworkListFilters = Map<NetworkListFilterType, Array<string>>;
export type NetworkPruneFilters = Map<NetworkPruneFilterType, Array<string>>;

export type NetworkListQueryOptions = {
    filters?: NetworkListFilters
};

export type NetworkPruneQueryOptions = {
    filters?: NetworkPruneFilters
};

export type NetworkListResult = Array<Network>;
type NetworkCreateResult = {
    Id: NetworkId,
    Warning: string
};
type NetworkPruneResult = {
    NetworksDeleted: Array<string>
};

export default class Networks {
    /**
     * @see https://docs.docker.com/engine/api/v1.44/#tag/Network/operation/NetworkList
     */
    static async list(modem: Modem, queryOptions: NetworkListQueryOptions = {}) {
        const query = new Map();
        if (queryOptions.filters) query.set("filters", JSON.stringify(Object.fromEntries(queryOptions.filters)));

        const call: ModemDialOptions = {
            path: "/networks",
            method: "GET",
            query: query
        };

        return await modem.dial(call) as NetworkListResult;
    }

    /**
     * @see https://docs.docker.com/engine/api/v1.44/#tag/Network/operation/NetworkCreate
     * 
     * @param modem the modem to use
     * @param spec the spec to use to create the network
     */
    static async create(modem: Modem, spec: NetworkSpec) {
        const call: ModemDialOptions = {
            path: "/networks/create",
            method: "POST",
            body: spec
        };

        const result = await modem.dial(call) as NetworkCreateResult;
        return new NetworkHandle(modem, result.Id);
    }

    static async prune(modem: Modem, queryOptions: NetworkPruneQueryOptions = {}) {
        const query = new Map();
        if (queryOptions.filters) query.set("filters", JSON.stringify(Object.fromEntries(queryOptions.filters)));

        const call: ModemDialOptions = {
            path: "/networks/prune",
            method: "POST",
            query: query
        };

        return await modem.dial(call) as NetworkPruneResult;
    }

    static getHandle(modem: Modem, id: NetworkId) {
        return new NetworkHandle(modem, id);
    }
}