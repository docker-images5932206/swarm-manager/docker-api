import Modem, { ModemDialOptions } from "../api/Modem";
import SecretHandle, { Secret, SecretId, SecretSpec } from "./secret";

type SecretListFilterType = "id" | "label" | "name" | "names";

export type SecretListFilters = Map<SecretListFilterType, Array<string>>;
export type SecretListResult = Array<Secret>;

export type SecretListQueryOptions = {
    filters?: SecretListFilters
};

type SecretCreateResult = {
    ID: SecretId
};

export default class Secrets {
    static async list(modem: Modem, queryOptions: SecretListQueryOptions = {}) {
        const query = new Map();
        if (queryOptions.filters) query.set("filters", JSON.stringify(Object.fromEntries(queryOptions.filters)));

        const call: ModemDialOptions = {
            path: "/secrets",
            method: "GET",
            query: query
        };

        return await modem.dial(call) as SecretListResult;
    }

    static async create(modem: Modem, spec: SecretSpec) {
        const call: ModemDialOptions = {
            path: "/secrets/create",
            method: "POST",
            body: spec
        };

        const result = await modem.dial(call) as SecretCreateResult;
        return new SecretHandle(modem, result.ID);
    }

    static getHandle(modem: Modem, id: SecretId) {
        return new SecretHandle(modem, id);
    }
}