import Modem, { ModemDialOptions } from "../api/Modem";
import { DOCKER_STACK_NAMESPACE_LABEL } from "../stack/stackDeps";
import Handle from "../utils/Handle";
import Stackable from "../utils/Stackable";
import { DockerLabels, Driver, SwarmBase } from "../utils/types";

export type SecretId = string;

interface SecretSpecBase {
    Name: string,
    Labels?: DockerLabels,
    Driver?: Driver,
    Templating?: Driver
}

export interface SecretSpec extends SecretSpecBase {
    Data: string,
};

export interface Secret extends SwarmBase<SecretId> {
    Spec: SecretSpecBase
}

export default class SecretHandle extends Handle implements Stackable {
    async inspect() {
        const call: ModemDialOptions = {
            path: "/secrets/" + this.id,
            method: "GET"
        };

        return await this.modem.dial(call) as Secret;
    }

    /**
     * @see https://docs.docker.com/engine/api/v1.44/#tag/Secret/operation/SecretUpdate
     * @param spec 
     */
    async update(spec: SecretSpec) {
        const current = await this.inspect();
        const version = current.Version.Index;

        const query = new Map();
        query.set("version", version);

        const newSpec = current.Spec;
        newSpec.Labels = spec.Labels;

        const call: ModemDialOptions = {
            path: "/secrets/" + this.id + "/update",
            method: "POST",
            query: query,
            body: newSpec
        };

        await this.modem.dial(call);
    }

    async remove() {
        const current = await this.inspect();

        if (this.shouldThrowStackError() && SecretHandle.isPartOfStack(current))
            throw new Error("The current secret was created by a stack, it can not be removed individually");

        const call: ModemDialOptions = {
            path: "/secrets/" + this.id,
            method: "DELETE"
        };

        await this.modem.dial(call);
    }

    public shouldThrowStackError() {
        return true;
    }

    static isPartOfStack(secret: Secret) {
        return secret.Spec.Labels && DOCKER_STACK_NAMESPACE_LABEL in secret.Spec.Labels;
    }

    static fromSecret(modem: Modem, secret: Secret) {
        const id = secret.ID;
        return new SecretHandle(modem, id);
    }
}