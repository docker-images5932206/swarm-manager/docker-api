import Modem from "../api/Modem";
import ConfigHandle, { Config } from "../conf/config";
import Configs from "../conf/configs";
import NetworkHandle, { Network } from "../network/network";
import Networks from "../network/networks";
import SecretHandle, { Secret } from "../secret/secret";
import Secrets from "../secret/secrets";
import ServiceHandle, { Service } from "../service/service";
import Services from "../service/services";
import Handle from "../utils/Handle";
import ClusterVolumeHandle, { ClusterVolume } from "../volume/clusterVolume";
import { Volume } from "../volume/volume";
import Volumes from "../volume/volumes";
import { DOCKER_STACK_NAMESPACE_LABEL } from "./stackDeps";

export type StackServices = Array<Service>;
export type StackConfigs = Array<Config>;
export type StackNetworks = Array<Network>;
export type StackSecrets = Array<Secret>;
export type StackVolumes = Array<Volume>;
export type StackClusterVolumes = Array<ClusterVolume>;

export type StackSpec = {
    Name: string,
    Yaml?: string
};

export interface Stack {
    Spec: StackSpec,
    Services: StackServices,
    Networks: StackNetworks,
    Configs: StackConfigs,
    Secrets: StackSecrets,
    Volumes: StackVolumes,
    ClusterVolumes: StackClusterVolumes
}

class StackServiceHandle extends ServiceHandle {
    public shouldThrowStackError() {
        return false;
    }

    public static fromService(modem: Modem, service: Service) {
        const id = service.ID;
        return new StackServiceHandle(modem, id);
    }
}

class StackNetworkHandle extends NetworkHandle {
    public shouldThrowStackError() {
        return false;
    }

    public static fromNetwork(modem: Modem, network: Network) {
        const id = network.Id;
        return new StackNetworkHandle(modem, id);
    }
}

class StackConfigHandle extends ConfigHandle {
    public shouldThrowStackError() {
        return false;
    }

    public static fromConfig(modem: Modem, config: Config) {
        const id = config.ID;
        return new StackConfigHandle(modem, id);
    }
}

class StackSecretHandle extends SecretHandle {
    public shouldThrowStackError() {
        return false;
    }

    public static fromSecret(modem: Modem, secret: Secret) {
        const id = secret.ID;
        return new StackSecretHandle(modem, id);
    }
}

class StackClusterVolumeHandle extends ClusterVolumeHandle {
    public shouldThrowStackError() {
        return false;
    }

    public static fromClusterVolume(modem: Modem, clusterVolume: ClusterVolume) {
        const id = clusterVolume.ClusterVolume.ID;
        return new StackClusterVolumeHandle(modem, id);
    }
}

export default class StackHandle extends Handle {
    public constructor(modem: Modem, name: string) {
        super(modem, name);
    }

    public async inspect(): Promise<Stack> {
        const labelFilter = new Map();
        labelFilter.set("label", Array.of(DOCKER_STACK_NAMESPACE_LABEL + "=" + this.id));

        const labelListQueryOptions = {
            filters: labelFilter
        };

        const serviceList = await Services.list(this.modem, labelListQueryOptions);
        const configList = await Configs.list(this.modem, labelListQueryOptions);
        const secretList = await Secrets.list(this.modem, labelListQueryOptions);
        const networkList = await Networks.list(this.modem, labelListQueryOptions);
        const volumeList = (await Volumes.list(this.modem, labelListQueryOptions)).Volumes;
        const clusterVolumeList = (await Volumes.listCluster(this.modem, labelListQueryOptions)).Volumes;

        return {
            Spec: {
                Name: this.id
            },
            Services: serviceList,
            Configs: configList,
            Secrets: secretList,
            Networks: networkList,
            Volumes: volumeList,
            ClusterVolumes: clusterVolumeList
        }
    }

    public async remove() {
        const stackFilter = new Map();
        stackFilter.set("label", Array.of(DOCKER_STACK_NAMESPACE_LABEL + "=" + this.id));

        const stackQueryOptions = {
            filters: stackFilter
        };

        // remove all services
        const services = await Services.list(this.modem, stackQueryOptions);
        const serviceHandles = services.map(service => { return StackServiceHandle.fromService(this.modem, service); });
        const servicePromises = serviceHandles.map(async handle => { await handle.remove(); });

        // IDEA maybe find a way to remove all local volumes on all nodes for the stack

        // remove networks
        const networks = await Networks.list(this.modem, stackQueryOptions);
        const networkHandles = networks.map(network => { return StackNetworkHandle.fromNetwork(this.modem, network); });
        const networkRemovePromises = networkHandles.map(async handle => { await handle.remove(); });

        // remove configs
        const configs = await Configs.list(this.modem, stackQueryOptions);
        const configHandles = configs.map(config => { return StackConfigHandle.fromConfig(this.modem, config); });
        const configRemovePromises = configHandles.map(async handle => { await handle.remove(); });

        // remove secrets
        const secrets = await Secrets.list(this.modem, stackQueryOptions);
        const secretHandles = secrets.map(secret => { return StackSecretHandle.fromSecret(this.modem, secret); });
        const secretRemovePromises = secretHandles.map(async handle => { await handle.remove(); });

        // remove cluster volumes
        const clusterVolumes = await Volumes.listCluster(this.modem, stackQueryOptions);
        const clusterVolumeHandles = clusterVolumes.Volumes.map(clusterVolume => { return StackClusterVolumeHandle.fromClusterVolume(this.modem, clusterVolume as ClusterVolume); });
        const clusterVolumePromises = clusterVolumeHandles.map(async handle => { await handle.remove(); });

        await Promise.all(servicePromises);
        await Promise.all(Array.of(
            ...networkRemovePromises,
            ...configRemovePromises,
            ...secretRemovePromises,
            ...clusterVolumePromises
        ));
    }
}