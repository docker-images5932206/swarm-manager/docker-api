import { ServiceSpec } from "../service/service";

const DOCKER_STACK_IMAGE_LABEL = "com.docker.stack.image";
export const DOCKER_STACK_NAMESPACE_LABEL = "com.docker.stack.namespace";