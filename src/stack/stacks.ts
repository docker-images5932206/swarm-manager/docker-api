import Modem from "../api/Modem";
import { Config } from "../conf/config";
import Configs, { ConfigListResult } from "../conf/configs";
import { Network } from "../network/network";
import Networks from "../network/networks";
import { Secret } from "../secret/secret";
import Secrets, { SecretListResult } from "../secret/secrets";
import { Service } from "../service/service";
import Services, { ServiceListResult } from "../service/services";
import { ClusterVolume } from "../volume/clusterVolume";
import Volumes from "../volume/volumes";
import StackHandle, { Stack, StackSpec } from "./stack";
import { DOCKER_STACK_NAMESPACE_LABEL } from "./stackDeps";
import { Volume } from "../volume/volume";

import { execFile } from 'child_process';
import { Readable } from "stream";

type StackObjectListResult = Promise<ServiceListResult | ConfigListResult | SecretListResult>;
type StackObject = Service | Config | Secret;

export type StackListResult = Array<Stack>;

function stackMapReducer(map: Map<string, Array<any>>, object: any): Map<string, Array<StackObject>> {
    if (!object.Labels || !(DOCKER_STACK_NAMESPACE_LABEL in object.Labels))
        return map;

    const stackName = object.Labels![DOCKER_STACK_NAMESPACE_LABEL];

    if (!map.has(stackName))
        map.set(stackName, new Array());

    map.get(stackName)!.push(object);
    return map;
}

function stackMapSpecReducer(map: Map<string, Array<StackObject>>, object: StackObject): Map<string, Array<StackObject>> {
    if (!object.Spec.Labels || !(DOCKER_STACK_NAMESPACE_LABEL in object.Spec.Labels))
        return map;

    const stackName = object.Spec.Labels![DOCKER_STACK_NAMESPACE_LABEL];

    if (!map.has(stackName))
        map.set(stackName, new Array());

    map.get(stackName)!.push(object);
    return map;
}

export default class Stacks {
    public static async list(modem: Modem) {
        const labelFilter = new Map();
        labelFilter.set("label", Array.of(DOCKER_STACK_NAMESPACE_LABEL));
        
        const labelListQueryOptions = {
            filters: labelFilter
        };

        const serviceListPromise = Services.list(modem, labelListQueryOptions) as StackObjectListResult;
        const configListPromise = Configs.list(modem, labelListQueryOptions) as StackObjectListResult;
        const secretListPromise = Secrets.list(modem, labelListQueryOptions) as StackObjectListResult;
        const networkListPromise = Networks.list(modem, labelListQueryOptions);
        const volumeListPromise = Volumes.list(modem, labelListQueryOptions);
        const clusterVolumeListPromise = Volumes.listCluster(modem, labelListQueryOptions);

        const promises = Array.of(
            serviceListPromise,
            configListPromise,
            secretListPromise
        );
        
        const results = await Promise.all(promises);
        const serviceMap: Map<string, Array<StackObject>> = results[0].reduce(stackMapSpecReducer, new Map());
        const configMap: Map<string, Array<StackObject>> = results[1].reduce(stackMapSpecReducer, new Map());
        const secretMap: Map<string, Array<StackObject>> = results[2].reduce(stackMapSpecReducer, new Map());

        const networkMap: Map<string, Array<Network>> = (await networkListPromise).reduce(stackMapReducer, new Map());
        const volumeMap: Map<string, Array<Volume>> = (await volumeListPromise).Volumes.reduce(stackMapReducer, new Map());
        const clusterVolumeMap: Map<string, Array<ClusterVolume>> = (await clusterVolumeListPromise).Volumes.reduce(stackMapReducer, new Map());
        
        const stackLabels = Array.of(
            ...Array.from(serviceMap.keys()),
            ...Array.from(configMap.keys()),
            ...Array.from(secretMap.keys()),
            ...Array.from(networkMap.keys()),
            ...Array.from(volumeMap.keys()),
            ...Array.from(clusterVolumeMap.keys())
        );
        
        const stackNames = new Set(stackLabels);
        const stacks = Array.from(stackNames).map<Stack>(name => {
            return {
                Spec: {
                    Name: name
                },
                Services: serviceMap.has(name) ? Array.from(serviceMap.get(name)!) : new Array(),
                Configs: configMap.has(name) ? Array.from(configMap.get(name)!) : new Array(),
                Secrets: secretMap.has(name) ? Array.from(secretMap.get(name)!) : new Array(),
                Networks: networkMap.has(name) ? Array.from(networkMap.get(name)!) : new Array(),
                Volumes: volumeMap.has(name) ? Array.from(volumeMap.get(name)!) : new Array(),
                ClusterVolumes: clusterVolumeMap.has(name) ? Array.from(clusterVolumeMap.get(name)!) : new Array()
            };
        });

        return stacks as StackListResult;
    }

    public static async create(modem: Modem, spec: StackSpec) {
        await new Promise(function(resolve, reject) {
            if (!spec.Yaml) {
                reject(new Error("Stack cannot be created without valid compose data"));
                return;
            }

            const child = execFile("docker", ["stack", "deploy", "-d", "--compose-file -", spec.Name], function(err, stdout, stderr) {
                if (err)
                    reject(err);

                // TODO maybe do something with the stdout and stderr
            });

            const stream = new Readable();
            stream.push(spec.Yaml);
            stream.push(null);

            if (child.stdin)
                stream.pipe(child.stdin);

            resolve(true);
        });
        
        return new StackHandle(modem, spec.Name);
    }

    public static getHandle(modem: Modem, name: string) {
        return new StackHandle(modem, name);
    }
}