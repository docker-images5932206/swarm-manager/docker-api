import Modem, { ModemDialOptions } from "../api/Modem";
import Handle from "../utils/Handle";
import { DockerLabels, EngineDescription, Platform, ResourceObject, SwarmBase, TLSInfo } from "../utils/types";

export type NodeId = string;

type NodeRole = "worker" | "manager";
type NodeAvailability = "active" | "pause" | "drain";

export type NodeSpec = {
    Name: string,
    Labels?: DockerLabels,
    Role: NodeRole,
    Availability: NodeAvailability
};

type NodeDescription = {
    Hostname: string,
    Platform: Platform,
    Resources: ResourceObject,
    Engine: EngineDescription,
    TLSInfo: TLSInfo
};

type NodeState = "unknown" | "down" | "ready" | "disconnected";
type NodeStatus = {
    State: NodeState,
    Message: string,
    Addr: string
};

type NodeManagerStatusReachability = "unknown" | "unreachable" | "reachable";
type NodeManagerStatus = {
    Leader: boolean,
    Reachability: NodeManagerStatusReachability,
    Addr: string
};

export interface Node extends SwarmBase<NodeId> {
    Spec: NodeSpec,
    Description: NodeDescription,
    Status: NodeStatus,
    ManagerStatus: NodeManagerStatus
};

export type NodeRemoveQueryOptions = {
    force?: boolean
};

export default class NodeHandle extends Handle {
    async inspect() {
        const call: ModemDialOptions = {
            path: "/nodes/" + this.id,
            method: "GET"
        };

        return await this.modem.dial(call) as Node;
    }

    async update(newSpec: NodeSpec) {
        const current = await this.inspect();
        const version = current.Version.Index;

        const query = new Map();
        query.set("version", version);

        const call: ModemDialOptions = {
            path: "/nodes/" + this.id + "/update",
            method: "POST",
            query: query,
            body: newSpec
        };

        await this.modem.dial(call);
    }

    async remove(queryOptions: NodeRemoveQueryOptions) {
        const query = new Map();
        query.set("force", queryOptions.force ? "true" : "false");

        const call: ModemDialOptions = {
            path: "/nodes/" + this.id,
            method: "DELETE",
            query: query
        };

        await this.modem.dial(call);
    }

    static fromNode(modem: Modem, node: Node) {
        const id = node.ID;
        return new NodeHandle(modem, id);
    }
}