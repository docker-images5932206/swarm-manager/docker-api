import Modem, { ModemDialOptions } from "../api/Modem";
import NodeHandle, { Node, NodeId } from "./node";

type NodeListFilterType = "id" | "label" | "membership" | "name" | "node.label" | "role";

export type NodeListFilters = Map<NodeListFilterType, Array<string>>;
export type NodeListResult = Array<Node>;

export type NodeListQueryOptions = {
    filters?: NodeListFilters
};

export default class Nodes {
    static async list(modem: Modem, queryOptions: NodeListQueryOptions = {}) {
        const query = new Map();
        if (queryOptions.filters) query.set("filters", JSON.stringify(Object.fromEntries(queryOptions.filters)));

        const call: ModemDialOptions = {
            path: "/nodes",
            method: "GET",
            query: query
        };

        return await modem.dial(call) as NodeListResult;
    }

    static getHandle(modem: Modem, id: NodeId) {
        return new NodeHandle(modem, id);
    }
}