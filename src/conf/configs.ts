import Modem, { ModemDialOptions } from "../api/Modem";
import ConfigHandle, { Config, ConfigId, ConfigSpec } from "./config";

type ConfigListFilterType = "id" | "label" | "name" | "names";

export type ConfigListFilters = Map<ConfigListFilterType, Array<string>>;
export type ConfigListResult = Array<Config>;

export type ConfigListQueryOptions = {
    filters?: ConfigListFilters
};

type ConfigCreateResult = {
    ID: ConfigId
};

export default class Configs {
    static async list(modem: Modem, queryOptions: ConfigListQueryOptions = {}) {
        const query = new Map();
        if (queryOptions.filters) query.set("filters", JSON.stringify(Object.fromEntries(queryOptions.filters)));

        const call: ModemDialOptions = {
            path: "/configs",
            method: "GET",
            query: query
        };

        return await modem.dial(call) as ConfigListResult;
    }

    static async create(modem: Modem, spec: ConfigSpec) {
        const call: ModemDialOptions = {
            path: "/configs/create",
            method: "POST",
            body: spec
        };

        const result = await modem.dial(call) as ConfigCreateResult;
        return new ConfigHandle(modem, result.ID);
    }

    static getHandle(modem: Modem, id: ConfigId) {
        return new ConfigHandle(modem, id);
    }
}