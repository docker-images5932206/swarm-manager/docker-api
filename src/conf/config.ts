import Modem, { ModemDialOptions } from "../api/Modem";
import { DOCKER_STACK_NAMESPACE_LABEL } from "../stack/stackDeps";
import Handle from "../utils/Handle";
import Stackable from "../utils/Stackable";
import { DockerLabels, Driver, SwarmBase } from "../utils/types";

export type ConfigId = string;

export type ConfigSpec = {
    Name: string,
    Labels?: DockerLabels,
    Data: string,
    Templating?: Driver
};

export interface Config extends SwarmBase<ConfigId> {
    Spec: ConfigSpec
}

export default class ConfigHandle extends Handle implements Stackable {
    async inspect() {
        const call: ModemDialOptions = {
            path: "/configs/" + this.id,
            method: "GET"
        };

        return await this.modem.dial(call) as Config;
    }

    /**
     * @see https://docs.docker.com/engine/api/v1.44/#tag/Config/operation/ConfigUpdate
     * @param spec 
     */
    async update(spec: ConfigSpec) {
        const current = await this.inspect();
        const version = current.Version.Index;

        const query = new Map();
        query.set("version", version);

        const newSpec = current.Spec;
        newSpec.Labels = spec.Labels;

        const call: ModemDialOptions = {
            path: "/configs/" + this.id + "/update",
            method: "POST",
            query: query,
            body: newSpec
        };

        await this.modem.dial(call);
    }

    async remove() {
        const current = await this.inspect();

        if (this.shouldThrowStackError() && ConfigHandle.isPartOfStack(current))
            throw new Error("The current config was created by a stack, it can not be removed individually");

        const call: ModemDialOptions = {
            path: "/configs/" + this.id,
            method: "DELETE"
        };

        await this.modem.dial(call);
    }

    public shouldThrowStackError() {
        return true;
    }

    static isPartOfStack(config: Config) {
        return config.Spec.Labels && DOCKER_STACK_NAMESPACE_LABEL in config.Spec.Labels;
    }

    static fromConfig(modem: Modem, config: Config) {
        const id = config.ID;
        return new ConfigHandle(modem, id);
    }
}