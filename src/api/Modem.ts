import https from "https";
import http from "http";

import fs from "fs";
import path from "path";

import { Socket } from "net";
import querystring from "node:querystring";

export type ModemDialMethod = "GET" | "POST" | "HEAD" | "PUT" | "DELETE" | "PATCH";
export type ModemDialResponseResult = object | null;
export type ModemDialStreamResult = http.IncomingMessage;
export type ModemDialResult = ModemDialResponseResult | ModemDialStreamResult | Socket;

interface ModemDialResultContentHandler { (raw: string): ModemDialResponseResult; }

export interface ModemDialOptions {
    path: string,
    method: ModemDialMethod,
    headers?: Map<string, string>,
    query?: Map<string, Array<string>>,
    body?: object | string,

    isStream?: boolean // CHECK if this is not needed in future api verisons
}

export default class Modem {
    private socketPath: string;
    private static version = "v1.44";

    private static responseStreamContentType = [
        "application/vnd.docker.raw-stream",
        "application/vnd.docker.multiplexed-stream"
    ];

    constructor(socketPath: string) {
        this.socketPath = socketPath;
    }

    private static responseIsStream(response: http.IncomingMessage): boolean {
        // CHECK maybe this can also be identified by the connection: keep-alive header
        const contentType = response.headers["content-type"];
        return contentType ? Modem.responseStreamContentType.includes(contentType) : false;
    }

    private static getResponseContentHandler(response: http.IncomingMessage): ModemDialResultContentHandler {
        switch (response.headers["content-type"]) {
            case "application/json": return (raw: string) => { return JSON.parse(raw); };
            default: return (raw: string) => null;
        }
    }

    private static getQueryString(query?: Map<string, Array<string>>) {
        return query && query.size > 0 ? "?" + querystring.stringify(Object.fromEntries(query)) : "";
    }

    public async dial(options: ModemDialOptions) {
        const socketPath = this.socketPath;
        const path = (Modem.version ? "/" + Modem.version : "") + options.path;

        const body = options.body ? JSON.stringify(options.body) : null;
        const headers: http.OutgoingHttpHeaders = options.headers ? Object.fromEntries(options.headers) : {}; // IDEA consider switching to https later
        
        if (body) {
            headers["Content-Type"] = "application/json";
            headers["Content-Length"] = Buffer.byteLength(body);
        }

        return await new Promise<ModemDialResult>(function(resolve, reject) {
            let finished = false;

            const requestOptions: http.RequestOptions = {
                socketPath: socketPath,
                path: path + Modem.getQueryString(options.query),
                method: options.method,
                headers: headers
            };

            const request = http.request(requestOptions);

            request.on("upgrade", function(response, socket, head) {
                if (finished)
                    return;

                finished = true;
                resolve(socket);
            });

            request.on("response", function(response) {
                const status = response.statusCode;

                if (!status) {
                    reject(new Error("Status is not defined"));
                    return;
                }

                if (Modem.responseIsStream(response) || options.isStream) {
                    if (finished)
                        return;

                    finished = true;
                    resolve(response);
                    return;
                }

                // CHECK it seems like the response does not always set content-length (e.g. /networks) maybe there is another way of telling
                // const contentLength = headers["content-length"] ? Number.parseInt(headers["content-length"]) : 0;
                const rawHandler = Modem.getResponseContentHandler(response);
                const isOk = status < 300;

                let chunks = new Array();

                const pushChunk = function(chunk: any) { chunks.push(chunk); };
                const responseEnd = function() {
                    if (finished)
                        return;

                    finished = true;

                    const buffer = Buffer.concat(chunks);
                    const raw = buffer.toString("utf-8");
                    const parsed = rawHandler(raw);

                    if (isOk)
                        resolve(parsed);
                    else
                        reject(parsed);
                };

                // response.setEncoding("utf-8");
                response.on("error", reject);
                response.on("data", pushChunk);
                response.on("end", responseEnd);
                response.on("close", responseEnd);
            });

            if (body)
                request.write(body);

            request.on("error", reject);
            request.end();
        }).catch(function(err) { return Promise.reject("message" in err ? err.message : err); });
    }
}