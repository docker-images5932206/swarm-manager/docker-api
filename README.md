# Docker API NodeJS

Since docker-modem is not really useable currently especially with the new cluster volumes, this project aims to integrate all of the features that docker-modem does not.

## Usage

### Setup

```typescript
import { Modem } from "docker-api-nodejs";

const modem = new Modem(<path-to-docker-socket>);
```

### Model

The project uses the responses from docker in a handle wrapper model.
That means that for a basic request such as listing object like volumes you will get the exact same response as you would when you call the socket directly.
The advantage is, that you don't have to write the wrappers for reusing basically the same code over and over yourself.
Other than that this project will include non docker native operations such as handling stack as for example docker-compose does.

If you want to list objects you can call the list function of the corresponding interface.

For example if you want to list all local volumes or clusterVolumes:

```typescript
const list = await Volumes.list(<modem>, <queryOptions>);
const clusterList = await Volumes.listCluster(<modem>, <queryOptions>);
```

The parameter "queryOptions" refers to an object that holds all parameters required by the docker socket. One such parameter can be 'filters' - a map object (Map&lt;string, Array&lt;string&gt;&gt;) that maps a string key to an array of string values (for more infos see [official docker api documentation](https://docs.docker.com/engine/api/v1.44)).

Hoever if you want to get specific information on an object e.g. inspecting its content more closely you can create a handle for that object like so:

```typescript
const volumeHandle = VolumeHandle.fromVolume(<modem>, <volume-object>);
const clusterVolumeHandle = Volumes.getClusterHandle(<modem>, <name of the clusterVolume>);

const handle = NodeHandle.fromNode(<modem>, <node-object>);
const handle = Nodes.getHandle(<modem>, <node-object-id>);
```

It may be needed to cast these ids to their respective types.

Create functions always return a handle on that specifc object, for example:

```typescript
const handle = await Configs.create(<modem>, <configSpec>);
```

From there on you can (if the object supports it) inspect, remove, update, ...

The parameters for every operation are based on the [official docker api documentation](https://docs.docker.com/engine/api/v1.44)

### Stacks

Stacks are handled a little different as there is no endpoint in the docker socket to connect to. See [here](src/stack/README.md) for more details

### Testing

Testing assumes a clean docker environment with no objects other than the default, also to complete all tests docker should be in swarm mode, as services, secrets, configs, ... do not exist in a non swarm environment. So if you want to test the project keep that in mind.  
To run tests use <code>npm run test</code>

## License

Copyright 2024 Benedikt Warmesbach

Licensed under the Apache license, version 2.0 (the "license"); You may not use this file except in compliance with the license. You may obtain a copy of the license at:

http://www.apache.org/licenses/LICENSE-2.0.html

Unless required by applicable law or agreed to in writing, software distributed under the license is distributed on an "as is" basis, without warranties or conditions of any kind, either express or implied. See the license for the specific language governing permissions and limitations under the license.