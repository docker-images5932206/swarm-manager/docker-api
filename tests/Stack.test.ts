import { ClusterVolumeSpec, ConfigSpec, Images, Modem, SecretSpec, ServiceSpec, StackHandle } from "../src";
import { Network, NetworkSpec } from "../src/network/network";
import Stacks from "../src/stack/stacks";

const image = "httpd";
const imageTag = "2.4.59";

describe("Stack", function() {
    const modem = new Modem("/var/run/docker.sock");
    const testStackName = "TestStack";

    test("list", async function() {
        const result = await Stacks.list(modem);
        expect(result).toHaveProperty("length");
    });

    test("inspect", async function() {
        const handle = new StackHandle(modem, "Test");
        const inspect = await handle.inspect();

        expect(inspect).toBeDefined();
        
        expect(inspect).toHaveProperty("Spec");
        expect(inspect.Spec).toHaveProperty("Name");
        expect(inspect.Spec.Name).toBe("Test");

        expect(inspect).toHaveProperty("Services");
        expect(inspect.Services).toHaveProperty("length");

        expect(inspect).toHaveProperty("Configs");
        expect(inspect.Configs).toHaveProperty("length");

        expect(inspect).toHaveProperty("Secrets");
        expect(inspect.Secrets).toHaveProperty("length");

        expect(inspect).toHaveProperty("Networks");
        expect(inspect.Networks).toHaveProperty("length");

        expect(inspect).toHaveProperty("ClusterVolumes");
        expect(inspect.ClusterVolumes).toHaveProperty("length");
    });

    test.todo("create");
    test.todo("remove");

    afterAll(async function() {
        const handle = Images.getHandle(modem, image + ":" + imageTag);
        await handle.remove().catch(err => null);
    });
});