import { Images, Modem } from "../src"
import PluginHandle from "../src/plugin/plugin";
import Plugins, { PluginInstallQueryOptions } from "../src/plugin/plugins";

describe("Plugin", function() {
    const modem = new Modem("/var/run/docker.sock");
    const options: PluginInstallQueryOptions = {
        remote: "ollijanatuinen/swarm-csi-smb:v1.13.0"
    };

    test.concurrent("list", async function() {
        const result = await Plugins.list(modem);
        expect(result).toBeDefined();
        expect(result).toHaveProperty("length");
    });

    test.concurrent("get privileges", async function() {
        const privileges = await Plugins.getPrivileges(modem, options);
        expect(privileges).toBeDefined();
        expect(privileges).toHaveProperty("length");
    });

    test.concurrent("install", async function() {
        const queryOptions = Object.assign({}, options, {
            name: "csi-smb-install",
        });

        const logWrapper = await Plugins.installGrantAllPrivileges(modem, queryOptions);
        const handle = logWrapper.Handle;
        expect(handle).toBeDefined();

        await handle.remove();
    }, 60000);

    test.concurrent("inspect", async function() {
        const queryOptions = Object.assign({}, options, {
            name: "csi-smb-inspect",
        });

        const logWrapper = await Plugins.installGrantAllPrivileges(modem, queryOptions);
        const handle = logWrapper.Handle;
        expect(handle).toBeDefined();

        const inspect0 = await handle.inspect();
        expect(inspect0.Name).toContain("csi-smb-inspect");
        expect(inspect0.Enabled).toBe(false);

        await handle.remove();
    }, 60000);

    test.concurrent("enable", async function() {
        const queryOptions = Object.assign({}, options, {
            name: "csi-smb-enable",
        });

        const logWrapper = await Plugins.installGrantAllPrivileges(modem, queryOptions);
        const handle = logWrapper.Handle;
        expect(handle).toBeDefined();

        await handle.enable();
        const inspect0 = await handle.inspect();
        expect(inspect0.Enabled).toBe(true);

        await handle.disable();
        const inspect1 = await handle.inspect();
        expect(inspect1.Enabled).toBe(false);

        await handle.remove();
    }, 60000);

    test.concurrent("remove", async function() {
        const queryOptions = Object.assign({}, options, {
            name: "csi-smb-remove",
        });

        const logWrapper = await Plugins.installGrantAllPrivileges(modem, queryOptions);
        const handle = logWrapper.Handle;
        expect(handle).toBeDefined();

        await handle.remove();
        await expect(handle.inspect()).rejects.toBeDefined();
    }, 60000);
});