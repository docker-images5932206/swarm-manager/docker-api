import { Modem } from "../src";
import NodeHandle from "../src/node/node";
import Nodes from "../src/node/nodes";

describe("Node", function() {
    const modem = new Modem("/var/run/docker.sock");

    test("list", async function() {
        const result = await Nodes.list(modem);
        expect(result).toHaveProperty("length");
    }, 15000);

    test("inspect", async function() {
        const listResult = await Nodes.list(modem);
        expect(listResult.length).toBeGreaterThanOrEqual(1);

        const node0 = listResult[0];
        const handle = NodeHandle.fromNode(modem, node0);
        const inspect = await handle.inspect();
        expect(inspect).toBeDefined();
    }, 10000);
});