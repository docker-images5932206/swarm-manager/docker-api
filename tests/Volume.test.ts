import Modem from "../src/api/Modem";
import { ClusterVolumeSpec } from "../src/volume/clusterVolume";
import { VolumeSpec } from "../src/volume/volume";
import Volumes, { VolumeListFilters } from "../src/volume/volumes";

describe("Volume", function() {
    const modem = new Modem("/var/run/docker.sock");

    test("list", async function() {
        const spec: VolumeSpec = {
            Name: "listTest"
        };

        const hanlde = await Volumes.create(modem, spec);

        const filters: VolumeListFilters = new Map();
        filters.set("name", Array.of("listTest"));

        const queryOptions = { filters: filters };

        const resultA = await Volumes.list(modem, queryOptions);
        expect(resultA).toBeDefined();
        expect(resultA.Volumes).toHaveProperty("length");

        filters.set("name", Array.of("tes1t"));

        const resultB = await Volumes.list(modem, queryOptions);
        expect(resultB).toBeDefined();
        expect(resultB.Volumes).toHaveProperty("length");

        await hanlde.remove();
        await expect(hanlde.inspect()).rejects.toBeDefined();
    });

    describe("create", function() {
        test("volume", async function() {
            const spec: VolumeSpec = {
                Name: "volumeTest"
            };

            const handle = await Volumes.create(modem, spec);;
            const inspect = await handle.inspect();
        
            expect(inspect.Name).toBe("volumeTest");
        
            await handle.remove();
            await expect(handle.inspect()).rejects.toBeDefined();
        });
        
        test("cluster volume", async function() {
            const spec: ClusterVolumeSpec = {
                Name: "clusterTest",
                ClusterVolumeSpec: {
                    Group: "test",
                    AccessMode: {
                        MountVolume: {}
                    }
                }
            };

            const handle = await Volumes.createCluster(modem, spec);
            const inspect = await handle.inspect();
        
            expect(inspect.Name).toBe("clusterTest");

            spec.ClusterVolumeSpec.Availability = "drain";
        
            await handle.update(spec);
            const update = await handle.inspect();
        
            expect(update.ClusterVolume.Spec.Availability).toBe("drain");
        
            await handle.remove();
            await expect(handle.inspect()).rejects.toBeDefined();
        });
    })

    test("prune local", async function() {
        const spec: VolumeSpec = {
            Name: "pruneTest"
        };

        await Volumes.create(modem, spec);

        const filters = new Map();
        filters.set("all", Array.of("true"));

        const queryOptions = { filters: filters };

        const result = await Volumes.prune(modem, queryOptions);
        const deletedVolumes = result.VolumesDeleted;

        expect(deletedVolumes).toHaveProperty("length");
        expect(deletedVolumes.length).toBeGreaterThanOrEqual(1);
        expect(deletedVolumes).toContain("pruneTest");
    }, 60000);
});