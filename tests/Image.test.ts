import { Images, Modem } from "../src"

describe("Image", function() {
    const modem = new Modem("/var/run/docker.sock");
    const imageQueryOptions = {
        fromImage: "hello-world",
        tag: "latest"
    }

    beforeAll(async function() {
        await Images.pull(modem, imageQueryOptions);
    }, 30000);

    test("list", async function() {
        const result = await Images.list(modem);
        expect(result).toHaveProperty("length");
    });

    // build image
    test.todo("prune builder cache");

    test("pull", async function() {
        const logWrapper = await Images.pull(modem, imageQueryOptions);
        const handle = logWrapper.Handle;

        expect(handle).toBeDefined();
        await handle.remove();
    }, 60000);

    test.todo("import");

    test("inspect", async function() {
        const logWrapper = await Images.pull(modem, imageQueryOptions);
        const handle = logWrapper.Handle;
        const inspect = await handle.inspect();

        expect(inspect).toBeDefined();
        expect(inspect).toHaveProperty("Id");
        expect(inspect).toHaveProperty("RepoTags");
        expect(inspect.RepoTags).toHaveProperty("length");
        expect(inspect.RepoTags).toContain("hello-world:latest");
    }, 60000);

    test.todo("history");
    test.todo("push");
    test.todo("tag");
    test.todo("remove");
    test.todo("search");

    test("prune", async function() {
        const filters = new Map();
        filters.set("dangling", Array.of("false"));
        
        const pruneResult = await Images.prune(modem, filters);

        expect(pruneResult).toBeDefined();
        expect(pruneResult).toHaveProperty("ImagesDeleted");
        expect(pruneResult).toHaveProperty("SpaceReclaimed");
    }, 60000);

    test.todo("commit");
});