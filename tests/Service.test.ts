import { Images, Modem } from "../src";
import { ServiceInspectQueryOptions, ServiceLogsQueryOptions, ServiceSpec } from "../src/service/service";
import Services from "../src/service/services";

const image = "httpd";
const imageTag = "2.4";

const updateImage = "httpd";
const updateImageTag = "2.3";

describe("Service", function() {
    const modem = new Modem("/var/run/docker.sock");
    const template: ServiceSpec = {
        TaskTemplate: {
            ContainerSpec: {
                Image: image + ":" + imageTag
            }
        }
    };

    beforeAll(async function() {
        await Images.pull(modem, {
            fromImage: image,
            tag: imageTag
        });

        await Images.pull(modem, {
            fromImage: updateImage,
            tag: updateImageTag
        });
    }, 60000);

    test("create", async function() {
        const createTemplate = Object.assign({}, template, {
            Name: "testService"
        });

        const handle = await Services.create(modem, createTemplate);
        expect(handle).toBeDefined();

        const inspectOptions: ServiceInspectQueryOptions = {
            insertDefaults: false
        };

        const inspect = await handle.inspect(inspectOptions);
        expect(inspect.Spec.Name).toBe("testService");
        expect(inspect.Spec.TaskTemplate.ContainerSpec?.Image).toBe(image + ":" + imageTag);

        await handle.remove();
        await expect(handle.inspect(inspectOptions)).rejects.toBeDefined();
    });

    test("list", async function() {
        const result = await Services.list(modem);
        expect(result).toHaveProperty("length");

        const createTemplate = Object.assign({}, template, {
            Name: "listTest"
        });

        const handle = await Services.create(modem, createTemplate);

        const afterCreate = await Services.list(modem);
        expect(afterCreate).toHaveProperty("length");
        expect(afterCreate.length).toBeGreaterThanOrEqual(1);
        expect(afterCreate.map(service => service.Spec.Name)).toContain("listTest");

        await handle.remove();
    });

    test("get logs", async function() {
        const logsTemplate = Object.assign({}, template, {
            Name: "logTest"
        });

        const handle = await Services.create(modem, logsTemplate);
        expect(handle).toBeDefined();

        const logQueryOptions: ServiceLogsQueryOptions = {
            stdout: true,
            stderr: true
        };

        const logStream = await handle.getLogs(logQueryOptions);
        expect(logStream).toBeDefined();

        await new Promise<void>(function(resolve) {
            setTimeout(async () => {
                logStream.destroy();
                await handle.remove();
                resolve();
            }, 1000);
        });
    }, 15000);

    test("update", async function() {
        const updateTemplate = Object.assign({}, template, {
            Name: "updateTest"
        });
        const handle = await Services.create(modem, updateTemplate);
        expect(handle).toBeDefined();

        const newTemplate: ServiceSpec = {
            Name: "updateTest",
            TaskTemplate: {
                ContainerSpec: {
                    Image: updateImage + ":" + updateImageTag
                }
            }
        };

        await handle.update(newTemplate);
        const inspect = await handle.inspect();
        expect(inspect.Spec.Name).toBe("updateTest");
        expect(inspect.Spec.TaskTemplate.ContainerSpec?.Image).toBe(updateImage + ":" + updateImageTag);

        await handle.remove();
        await expect(handle.inspect()).rejects.toBeDefined();
    });

    test("redeploy", async function() {
        const redeployTemplate = Object.assign({}, template, {
            Name: "redeployTest"
        });
        const handle = await Services.create(modem, redeployTemplate);
        expect(handle).toBeDefined();

        const newHandle = await handle.redeploy();
        const inspect = await newHandle.inspect();

        expect(inspect.Spec.Name).toBe("redeployTest");
        expect(inspect.Spec.TaskTemplate.ContainerSpec?.Image).toBe(image + ":" + imageTag);

        await newHandle.remove();

        await expect(handle.inspect()).rejects.toBeDefined();
        await expect(newHandle.inspect()).rejects.toBeDefined();
    });

    test("rollback", async function() {
        const preRollbackTemplate = Object.assign({}, template, {
            Name: "rollbackTest"
        });
        const handle = await Services.create(modem, preRollbackTemplate);
        expect(handle).toBeDefined();

        const rollbackTemplate: ServiceSpec = {
            Name: "rollbackTest",
            TaskTemplate: {
                ContainerSpec: {
                    Image: updateImage + ":" + updateImageTag
                }
            }
        };

        await handle.update(rollbackTemplate);
        const inspect = await handle.inspect();
        expect(inspect.Spec.Name).toBe("rollbackTest");
        expect(inspect.Spec.TaskTemplate.ContainerSpec?.Image).toBe(updateImage + ":" + updateImageTag);
        
        await new Promise(resolve => {
            setTimeout(resolve, 1000);
        });

        await handle.rollback();
        const afterRollback = await handle.inspect();
        expect(afterRollback.Spec.Name).toBe("rollbackTest");
        expect(afterRollback.Spec.TaskTemplate.ContainerSpec?.Image).toBe(image + ":" + imageTag);

        await handle.remove();
        await expect(handle.inspect()).rejects.toBeDefined();
    });

    test.todo("stop");
    test.todo("start");
    test.todo("remove");

    afterAll(async function() {
        const handle = Images.getHandle(modem, image + ":" + imageTag);
        const updateHandle = Images.getHandle(modem, updateImage + ":" + updateImageTag);
        
        await handle.remove().catch(err => null);
        await updateHandle.remove().catch(err => null);
    }, 30000);
});