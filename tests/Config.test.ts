import { Modem } from "../src";
import { ConfigSpec } from "../src/conf/config";
import Configs from "../src/conf/configs";

describe("Config", function() {
    const modem = new Modem("/var/run/docker.sock");
    const testData = Buffer.from("test", "binary").toString("base64");

    test("create", async function() {
        const spec: ConfigSpec = {
            Name: "createTest",
            Data: testData
        };

        const handle = await Configs.create(modem, spec);
        expect(handle).toBeDefined();

        const inspect = await handle.inspect();
        expect(inspect.Spec.Name).toBe("createTest");
        expect(Buffer.from(inspect.Spec.Data, "base64").toString("binary")).toBe("test");

        await handle.remove();
        await expect(handle.inspect()).rejects.toBeDefined();
    });

    test("list", async function() {
        const list0 = await Configs.list(modem);
        expect(list0).toHaveProperty("length");

        const spec: ConfigSpec = {
            Name: "listTest",
            Data: testData
        };

        const handle = await Configs.create(modem, spec);
        const list1 = await Configs.list(modem);
        expect(list1).toHaveLength(list0.length + 1);

        await handle.remove();
    });

    test("update", async function() {
        const spec: ConfigSpec = {
            Name: "updateTest",
            Data: testData
        };

        const handle = await Configs.create(modem, spec);
        expect(handle).toBeDefined();

        const newSpec: ConfigSpec = {
            Name: "updateTest2",
            Data: Buffer.from("test2", "binary").toString("base64"),
            Labels: {
                "test": "test"
            }
        }

        await handle.update(newSpec);
        const inspect = await handle.inspect();
        expect(inspect.Spec.Name).toBe("updateTest");
        expect(Buffer.from(inspect.Spec.Data, "base64").toString("binary")).toBe("test");
        expect(inspect.Spec.Labels).toBeDefined();
        expect(inspect.Spec.Labels!["test"]).toBe("test");

        await handle.remove();
        await expect(handle.inspect()).rejects.toBeDefined();
    });
});