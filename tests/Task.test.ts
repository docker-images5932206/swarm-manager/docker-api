import { Images, Modem, Services } from "../src";
import { ServiceSpec } from "../src/service/service";
import Tasks from "../src/task/tasks";

const image = "httpd:2.4.60";

describe("Task", function() {
    const modem = new Modem("/var/run/docker.sock");

    test("list", async function() {
        const result = await Tasks.list(modem);
        expect(result).toBeDefined();
        expect(result).toHaveProperty("length");

        const testSpec: ServiceSpec = {
            TaskTemplate: {
                ContainerSpec: {
                    Image: image
                }
            }
        };

        const serviceHandle = await Services.create(modem, testSpec);
        expect(serviceHandle).toBeDefined();

        const afterServiceCreate = await Tasks.list(modem);
        expect(afterServiceCreate).toBeDefined();
        expect(afterServiceCreate).toHaveProperty("length");

        await serviceHandle.remove();

        const afterDelete = await Tasks.list(modem);
        expect(afterDelete).toBeDefined();
        expect(afterDelete).toHaveProperty("length");
    }, 10000);

    afterAll(async function() {
        const handle = Images.getHandle(modem, image);
        await handle.remove().catch(err => null);
    });
});