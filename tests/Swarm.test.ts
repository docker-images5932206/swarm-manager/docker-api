import { Modem } from "../src";
import Swarm from "../src/swarm/swarm";

describe("Swarm", function() {
    const modem = new Modem("/var/run/docker.sock");

    test("inspect", async function() {
        const result = await Swarm.inspect(modem);
        expect(result).toBeDefined();
    });

    test.todo("init");
    test.todo("join");
    test.todo("leave");
    test.todo("update");
    test.todo("get unlock key");
    test.todo("unlock");
});