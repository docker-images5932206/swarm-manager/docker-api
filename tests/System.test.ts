import { Modem } from "../src";
import System from "../src/system/system";

describe("System", function() {
    const modem = new Modem("/var/run/docker.sock");

    test("info", async function() {
        const info = await System.info(modem);
        expect(info).toBeDefined();
        expect(info.ID).toBeDefined();
    });

    test("version", async function() {
        const version = await System.version(modem);
        expect(version).toBeDefined();
    });

    test("usage data", async function() {
        const data = await System.usageData(modem);
        expect(data).toBeDefined();
        expect(data.Volumes).toHaveProperty("length");
    }, 30000);

    test("monitor events", async function() {
        const events = await System.monitorEvents(modem, {
            since: Date.now().toString()
        });
        expect(events).toBeDefined();

        await new Promise<void>(resolve => {
            setTimeout(() => {
                events.destroy();
                resolve(); 
            }, 2000);
        });
    }, 10000);
});