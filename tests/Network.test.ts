import { Modem } from "../src";
import { NetworkSpec } from "../src/network/network";
import Networks from "../src/network/networks";

describe("Network", function() {
    const modem = new Modem("/var/run/docker.sock");

    test("list", async function() {
        const result = await Networks.list(modem);
        expect(result).toHaveProperty("length");
        expect(result.length).toBeGreaterThanOrEqual(3);

        const spec: NetworkSpec = {
            Name: "listTest"
        };

        const handle = await Networks.create(modem, spec);
        await expect(Networks.list(modem)).resolves.toHaveLength(result.length + 1);

        await handle.remove();
    }, 25000);

    test("create", async function() {
        const spec: NetworkSpec = {
            Name: "createTest"
        };

        const handle = await Networks.create(modem, spec);
        expect(handle).toBeDefined();

        const inspect = await handle.inspect();
        expect(inspect.Name).toBe("createTest");

        await handle.remove();
        await expect(handle.inspect()).rejects.toBeDefined();
    }, 20000);

    test.todo("remove");
    test.todo("connect");
    test.todo("disconnect");

    test("prune", async function() {
        const spec: NetworkSpec = {
            Name: "pruneTest"
        };

        await Networks.create(modem, spec);

        const pruneResult = await Networks.prune(modem);
        const deletedNetworks = pruneResult.NetworksDeleted;
        
        expect(deletedNetworks).toContain("pruneTest");
        expect(deletedNetworks).toHaveProperty("length");
        expect(deletedNetworks.length).toBeGreaterThanOrEqual(1);
    }, 30000);
});