import { Modem } from "../src";
import { SecretSpec } from "../src/secret/secret";
import Secrets from "../src/secret/secrets";

describe("Secret", function() {
    const modem = new Modem("/var/run/docker.sock");
    const testData = Buffer.from("test", "binary").toString("base64");

    test("create", async function() {
        const spec: SecretSpec = {
            Name: "createTest",
            Data: testData
        };

        const handle = await Secrets.create(modem, spec);
        expect(handle).toBeDefined();

        const inspect = await handle.inspect();
        expect(inspect.Spec.Name).toBe("createTest");

        await handle.remove();
        await expect(handle.inspect()).rejects.toBeDefined();
    });

    test("list", async function() {
        const list0 = await Secrets.list(modem);
        expect(list0).toHaveProperty("length");

        const spec: SecretSpec = {
            Name: "listTest",
            Data: testData
        };

        const handle = await Secrets.create(modem, spec);
        const list1 = await Secrets.list(modem);
        expect(list1).toHaveLength(list0.length + 1);

        await handle.remove();
    });

    test("update", async function() {
        const spec: SecretSpec = {
            Name: "updateTest",
            Data: testData
        };

        const handle = await Secrets.create(modem, spec);
        expect(handle).toBeDefined();

        const newSpec: SecretSpec = {
            Name: "updateTest2",
            Data: Buffer.from("test2", "binary").toString("base64"),
            Labels: {
                "test": "test"
            }
        }

        await handle.update(newSpec);
        const inspect = await handle.inspect();
        expect(inspect.Spec.Name).toBe("updateTest");
        expect(inspect.Spec.Labels).toBeDefined();
        expect(inspect.Spec.Labels!["test"]).toBe("test");

        await handle.remove();
        await expect(handle.inspect()).rejects.toBeDefined();
    });
});