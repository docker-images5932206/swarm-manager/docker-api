import Modem, { ModemDialOptions } from "../src/api/Modem";

describe("Modem", function() {
    const modem = new Modem("/var/run/docker.sock");

    test("Modem", async function() {
        const options: ModemDialOptions = {
            path: "/_ping",
            method: "GET"
        };

        const pingData = await modem.dial(options);
        expect(pingData).toBeDefined();
    });
});