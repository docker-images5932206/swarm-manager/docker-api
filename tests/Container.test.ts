import { Containers, ContainerSpec, Images, Modem } from "../src";

const image = "httpd";
const imageTag = "2.4.61";

describe("Container", function() {
    const modem = new Modem("/var/run/docker.sock");
    const spec: ContainerSpec = {
        Image: image + ":" + imageTag
    };

    beforeAll(async function() {
        await Images.pull(modem, {
            fromImage: image,
            tag: imageTag
        });
    }, 60000);

    test("list", async function() {
        const list0 = await Containers.list(modem);

        expect(list0).toBeDefined();
        expect(list0).toHaveProperty("length");
        expect(list0.length).toBeGreaterThanOrEqual(0);

        const handle = await Containers.create(modem, spec);

        const list1 = await Containers.list(modem);
        expect(list1).toBeDefined();
        expect(list1).toHaveProperty("length");
        expect(list1.length).toBeGreaterThanOrEqual(1);

        await handle.remove({ force: true });
    }, 20000);

    test("create", async function() {
        const handle = await Containers.create(modem, spec);
        expect(handle).toBeDefined();

        await handle.remove({ force: true });
    }, 30000);

    test("inspect", async function() {
        const handle = await Containers.create(modem, spec);

        const inspect = await handle.inspect();
        expect(inspect).toBeDefined();
        expect(inspect).toHaveProperty("Image");
        expect(inspect.Image).toBeDefined();

        await handle.remove({ force: true });
    }, 30000);

    test.todo("list processes");

    test("get logs", async function() {
        const handle = await Containers.create(modem, spec);
        const logsStream = await handle.getLogs({ stdout: true, stderr: true, follow: true, timestamps: true });

        expect(logsStream).toBeDefined();

        await handle.remove({ force: true });
    });

    test.todo("changes");
    test.todo("export");

    test("stats", async function() {
        const handle = await Containers.create(modem, spec);
        const statStream = await handle.stats({ stream: true });

        expect(statStream).toBeDefined();

        await handle.remove({ force: true });
    });

    test.todo("resize");
    test.todo("start");
    test.todo("stop");
    test.todo("restart");
    test.todo("kill");
    test.todo("update");
    test.todo("rename");
    test.todo("pause");
    test.todo("unpause");
    test.todo("wait");

    test("remove", async function() {
        const handle = await Containers.create(modem, spec);

        await handle.remove({ force: true });
        await expect(handle.inspect()).rejects.toBeDefined();
    }, 30000);

    test("prune", async function() {
        await Containers.create(modem, spec);
        const result = await Containers.prune(modem);

        expect(result).toBeDefined();
        expect(result).toHaveProperty("ContainersDeleted");
        expect(result).toHaveProperty("SpaceReclaimed");
    }, 60000);

    afterAll(async function() {
        const handle = Images.getHandle(modem, image + ":" + imageTag);
        await handle.remove().catch(err => null);
    });
});